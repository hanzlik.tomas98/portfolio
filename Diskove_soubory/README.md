# Zápočtový program pro předmět Algoritmizace a programování v Java

## Zadání
V jazyce Java implementovat program orientovaný na manipulaci daty s diskovými soubory

## info
Celé zadání a shrnutí postupu & výstupu je v souboru Hanzlik_alp2s_dokumentace.pdf
