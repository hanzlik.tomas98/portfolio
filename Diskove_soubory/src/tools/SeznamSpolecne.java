package tools;

/**
 *
 * @author hanzl
 */
public class SeznamSpolecne {

    protected String nazev;
    protected int rokVydani;

    /**
     * 
     * @param nazev
     * @param rokVydani 
     */
    public SeznamSpolecne(String nazev, int rokVydani) {
        this.nazev = nazev;
        this.rokVydani = rokVydani;
    }

    public String getNazev() {
        return nazev;
    }

    public int getRokVydani() {
        return rokVydani;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public void setRokVydani(int rokVydani) {
        this.rokVydani = rokVydani;
    }
    
}
