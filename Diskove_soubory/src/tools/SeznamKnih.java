package tools;

import java.util.ArrayList;

/**
 *
 * @author hanzl
 */
public class SeznamKnih extends SeznamSpolecne implements Comparable<SeznamKnih>{
    
    public static int volbaTrideni;
    
    //"﻿nazev;autor;rok_vydani;druh;zanr";
    //private String nazev; 
    //private int rokVydani;
    private String autor;
    private String druh;
    private String zanr;
    
    /**
     *
     * @param nazev
     * @param autor
     * @param rokVydani
     * @param druh
     * @param zanr
     */
    public SeznamKnih(String nazev, String autor, int rokVydani, String druh, String zanr) {
        super(nazev, rokVydani);
        this.autor = autor;
        this.druh = druh;
        this.zanr = zanr;
    }

    public String getAutor() {
        return autor;
    }

    public String getDruh() {
        return druh;
    }

    public String getZanr() {
        return zanr;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public void setDruh(String druh) {
        this.druh = druh;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }
    /*public static SeznamKnih rucniVstup(){
     SeznamKnih tmp = new SeznamKnih(null, null, 0, null, null);
     tmp.setAutor();
        
        
     }*/

    public static ArrayList<String> seznamAtributu() {
        //nazev;autor;rok_vydani;druh;zanr
        ArrayList<String> tmpList = new ArrayList<>(5);
        
        tmpList.add("nazev");
        tmpList.add("autor");
        tmpList.add("rok_vydani");
        tmpList.add("druh");
        tmpList.add("zanr");

        return tmpList;
    }

    public void nastavAtributy(String idAtributu, String vstup) {

        switch (idAtributu) {
            case "nazev":
                setNazev(vstup);
                break;
                
            case "autor":
                setAutor(vstup);
                break;
            
            case "rok_vydani":
                setRokVydani(Integer.parseInt(vstup));
                break;
                
            case "druh":
                setDruh(vstup);
                break;
                
            case "zanr":
                setZanr(vstup);
                break;
        }
    }
    public String prectiAtributy(String idAtributu){
        switch (idAtributu) {
            case "nazev":
                return getNazev();
                
                
            case "autor":
                return getAutor();
               
                
            case "rok_vydani":
                return Integer.toString(getRokVydani());
                
                
            case "druh":
                return getDruh();
                
            case "zanr":
                return getZanr();
                
            default:
                return null;
                   
        }
    }

    @Override
    public int compareTo(SeznamKnih o) {
        switch( volbaTrideni ){
            case 1: // by nazev
                return this.getNazev().compareToIgnoreCase(o.getNazev());
            case 2: // by rok
                return this.getRokVydani() - o.getRokVydani();
            default:
                return this.getNazev().compareToIgnoreCase(o.getNazev());
        }
    }
}
