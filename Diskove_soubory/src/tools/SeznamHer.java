package tools;

import java.util.ArrayList;

/**
 *
 * @author hanzl
 */
public class SeznamHer extends SeznamSpolecne implements Comparable<SeznamHer>{

    public static int volbaTrideni;
    
    //private String nazev;  
    //private int rokVydani; 
    private String vydavatel;
    private String zanr;

    /**
     *
     * @param nazev
     * @param vydavatel
     * @param rokVydani
     * @param zanr
     */
    public SeznamHer(String nazev, String vydavatel, int rokVydani, String zanr) {
        super(nazev, rokVydani);
        this.vydavatel = vydavatel;
        this.zanr = zanr;
    }

    public String getVydavatel() {
        return vydavatel;
    }

    public String getZanr() {
        return zanr;
    }

    public void setVydavatel(String vydavatel) {
        this.vydavatel = vydavatel;
    }

    public void setZanr(String zanr) {
        this.zanr = zanr;
    }
    
    public static ArrayList<String> seznamAtributu() {
        //nazev;vydavatel;rok_vydani;zanr
        ArrayList<String> tmpList = new ArrayList<>(4);
        tmpList.add("nazev");
        tmpList.add("vydavatel");
        tmpList.add("rok_vydani");
        tmpList.add("zanr");

        return tmpList;
    }

    public void nastavAtributy(String idAtributu, String vstup) {
       
        switch (idAtributu) {
            case "nazev":
                setNazev(vstup);
                break;
                
            case "vydavatel":
                setVydavatel(vstup);
                break;
                
            case "rok_vydani":               
                setRokVydani(Integer.parseInt(vstup));
                break;
                
            case "zanr":
                setZanr(vstup);
                break;    
        }
    }
    public String prectiAtributy(String idAtributu){
        switch (idAtributu) {
            case "nazev":
                return getNazev();
                
                
            case "vydavatel":
                return getVydavatel();
               
                
            case "rok_vydani":
                return Integer.toString(getRokVydani());
              
             
            case "zanr":
                return getZanr();
                
            default:
                return null;
        }
    }

    @Override
    public int compareTo(SeznamHer o) {
        switch( volbaTrideni ){
            case 1: // by nazev
                return this.getNazev().compareToIgnoreCase(o.getNazev());
            case 2: // by rok
                return this.getRokVydani() - o.getRokVydani();
            default:
                return this.getNazev().compareToIgnoreCase(o.getNazev());
        }
    }
}
