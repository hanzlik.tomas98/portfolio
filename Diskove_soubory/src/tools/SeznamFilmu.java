package tools;

import java.util.ArrayList;

/**
 *
 * @author hanzl
 */
public class SeznamFilmu extends SeznamSpolecne implements Comparable<SeznamFilmu>{
    //nazev;anglicky_nazev;rok_vydani;url_odkaz"
    //private String nazev;     
    //private int rokVydani; 
    public static int volbaTrideni;
    
    private String anglickyNazev;
    private String urlOdkaz;

    public SeznamFilmu(String nazev, String anglickyNazev, int rokVydani, String urlOdkaz) {
        super(nazev, rokVydani);
        this.anglickyNazev = anglickyNazev;
        this.urlOdkaz = urlOdkaz;
    }

    public String getAnglickyNazev() {
        return anglickyNazev;
    }

    public String getUrlOdkaz() {
        return urlOdkaz;
    }

    public void setAnglickyNazev(String anglickyNazev) {
        this.anglickyNazev = anglickyNazev;
    }

    public void setUrlOdkaz(String urlOdkaz) {
        this.urlOdkaz = urlOdkaz;
    }

    public static ArrayList<String> seznamAtributu() {
        //nazev;anglicky_nazev;rok_vydani;url_odkaz
        ArrayList<String> tmpList = new ArrayList<>(4);
        tmpList.add("nazev");
        tmpList.add("anglicky_nazev");
        tmpList.add("rok_vydani");
        tmpList.add("url_odkaz");

        return tmpList;
    }

    public void nastavAtributy(String idAtributu, String vstup) {

        switch (idAtributu) {
            case "nazev":
                setNazev(vstup);
                break;
                
            case "anglicky_nazev":
                setAnglickyNazev(vstup);
                break;
                
            case "rok_vydani":
                setRokVydani(Integer.parseInt(vstup));
                break;
                
            case "url_odkaz":
                setUrlOdkaz(vstup);
                break;

        }
    }
    public String prectiAtributy(String idAtributu){
        switch (idAtributu) {
            case "nazev":
                return getNazev();
                
            case "anglicky_nazev":
                return getAnglickyNazev();
                
            case "rok_vydani":
                return Integer.toString(getRokVydani());
                
            case "url_odkaz":
                return getUrlOdkaz();
                
            default:               
                return null;

        }
    }

    @Override
    public int compareTo(SeznamFilmu o) {
        switch( volbaTrideni ){
            case 1: // by nazev
                return this.getNazev().compareToIgnoreCase(o.getNazev());
            case 2: // by rok
                return this.getRokVydani() - o.getRokVydani();
            default:
                return this.getNazev().compareToIgnoreCase(o.getNazev());
        }
    }
}
