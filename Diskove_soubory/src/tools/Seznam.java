package tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author hanzl
 */
public class Seznam {

    private ArrayList<SeznamHer> seznamHer;
    private ArrayList<SeznamKnih> seznamKnih;
    private ArrayList<SeznamFilmu> seznamFilmu;
    //druh - epika/lyrika/drama
    //udelat napovedu? 
    private final static String hryHeader = "nazev;vydavatel;rok_vydani;zanr";
    private final static String filmyHeader = "nazev;anglicky_nazev;rok_vydani;url_odkaz";
    private final static String knihyHeader = "nazev;autor;rok_vydani;druh;zanr";
    private final static String separator = ";";

    //v arraylistu:
    //Hry: Nazev,vydavatel, rok vydani, zanr, popis
    //Knihy: Nazev, autor, rok vydani, zanr, popis
    //filmy: Nazev,ENG nazev, rok vydani, csfd cislo odkazu
    //https://www.csfd.cz/film/ + kladne cele cislo    
    //edit: nechat uživatele zkopírovat celou adresu
    //(udělat jeden list s atributem typu hry/kniha/film?)
    //nacitani? Cteni a formatovani stringu?
    //zajistit moznost pro vyber cesty k souborum
    //priznak pri ukonceni - ulozeni zmen?
    private Seznam(ArrayList seznamHer, ArrayList seznamKnih, ArrayList seznamFilmu) {
        this.seznamHer = seznamHer;
        this.seznamKnih = seznamKnih;
        this.seznamFilmu = seznamFilmu;
    }

    /**
     * Tovarni metoda,poskytuje nove vytvorenou instanci. Nove vytvorena
     * instance ma seznamy nactene ze souboru
     *
     * @param jmSouborHry
     * @param jmSouborKnihy
     * @param jmSouborFilmy
     * @return 
     * @throws java.io.IOException
     */
    public static Seznam getInstance(String jmSouborHry, String jmSouborKnihy, String jmSouborFilmy) throws IOException {

        return new Seznam(getCsvSoubor(jmSouborHry, separator, hryHeader, "seznamHer"),
                getCsvSoubor(jmSouborKnihy, separator, knihyHeader, "seznamKnih"),
                getCsvSoubor(jmSouborFilmy, separator, filmyHeader, "seznamFilmu"));

    }

    private static ArrayList getCsvSoubor(String jmSouboru, String separator, String hlavickaCsv, String nazevObjektu) throws IOException {

        ArrayList mujArrayList = new ArrayList<>();
        try {
            try (BufferedReader br = new BufferedReader(new FileReader(jmSouboru))) {

                // kontrola správnosti souboru,
                // tj. zda první řádek obsahuje očekávané hodnoty
                String line = br.readLine();
                if (line == null || !line.equals(hlavickaCsv)) {               
                    throw new IOException(apps.SeznamApp.rb.getString("IOErrFormat") + jmSouboru + apps.SeznamApp.rb.getString("IOErrHeader") + hlavickaCsv);
                } else {
                    // hryHeader = "nazev;vydavatel;rok_vydani;zanr";
                    // filmyHeader = "﻿nazev;anglicky_nazev;rok_vydani;url_odkaz";
                    // knihyHeader = "﻿nazev;autor;rok_vydani;druh;zanr";               
                    while ((line = br.readLine()) != null && !line.isEmpty()) {
                        String[] items = line.split(separator);
                        switch (nazevObjektu) {
                            case "seznamHer":
                                mujArrayList.add(new SeznamHer(items[0], items[1], Integer.parseInt(items[2]), items[3]));
                                break;
                            case "seznamKnih":
                                mujArrayList.add(new SeznamKnih(items[0], items[1], Integer.parseInt(items[2]), items[3], items[4]));
                                break;
                            case "seznamFilmu":
                                mujArrayList.add(new SeznamFilmu(items[0], items[1], Integer.parseInt(items[2]), items[3]));
                                break;
                        }

                        //kontrola vypisu
                    /*System.out.print("[");
                         for (int i = 0; i < items.length; i++) {
                         if (i > 0) {
                         System.out.print("-");
                         }
                         System.out.print(items[i]);
                         }
                         System.out.println("]");*/
                    }
                }
            }
        } catch (java.io.FileNotFoundException e) {
            //pri spusteni programu nemusi existovat zadany soubor           
        }
        return mujArrayList;

    }

    /*public void pridejDoSeznamuKnih(String nazev, String autor, int rokVydani, String druh, String zanr) {
     SeznamKnih novyZaznam;        
     novyZaznam = new SeznamKnih(nazev, autor, rokVydani, druh, zanr);            
     seznamKnih.add(novyZaznam);        
        
     }*/
    /**
     * Prida objekt nove polozky do seznamu
     *
     * @param novaPolozka
     */
    public void pridejDoSeznamuHer(SeznamHer novaPolozka) {
        seznamHer.add(novaPolozka);
    }

    /**
     * Prida objekt nove polozky do seznamu
     *
     * @param novaPolozka
     */
    public void pridejDoSeznamuKnih(SeznamKnih novaPolozka) {
        seznamKnih.add(novaPolozka);
    }

    /**
     * Prida objekt nove polozky do seznamu
     *
     * @param novaPolozka
     */
    public void pridejDoSeznamuFilmu(SeznamFilmu novaPolozka) {
        seznamFilmu.add(novaPolozka);
    }

    /**
     * Vypise aktualni seznam
     *
     * @param separator - zpusob oddeleni polozek
     * @param showID - ukazani poradi polozek seznamu
     * @return
     */
    public String getVypisHry(String separator, boolean showID) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListHra = SeznamHer.seznamAtributu();
        for (int i = 0; i < seznamHer.size(); i++) {
            if (showID == true) {
                sb.append(i).append(". ");
            }
            for (int j = 0; j < tmpListHra.size(); j++) {
                sb.append(seznamHer.get(i).prectiAtributy(tmpListHra.get(j)));
                if (j < tmpListHra.size() - 1) {
                    sb.append(separator);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Vypise aktualni seznam
     *
     * @param separator - zpusob oddeleni polozek
     * @param showID - ukazani poradi polozek seznamu
     * @return
     */
    public String getVypisKnihy(String separator, boolean showID) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListKniha = SeznamKnih.seznamAtributu();
        for (int i = 0; i < seznamKnih.size(); i++) {
            if (showID == true) {
                sb.append(i).append(". ");
            }
            for (int j = 0; j < tmpListKniha.size(); j++) {
                sb.append(seznamKnih.get(i).prectiAtributy(tmpListKniha.get(j)));
                if (j < tmpListKniha.size() - 1) {
                    sb.append(separator);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Vypise aktualni seznam
     *
     * @param separator - zpusob oddeleni polozek
     * @param showID - ukazani poradi polozek seznamu
     * @return
     */
    public String getVypisFilmy(String separator, boolean showID) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListFilm = SeznamFilmu.seznamAtributu();
        for (int i = 0; i < seznamFilmu.size(); i++) {
            if (showID == true) {
                sb.append(i).append(". ");
            }
            for (int j = 0; j < tmpListFilm.size(); j++) {
                sb.append(seznamFilmu.get(i).prectiAtributy(tmpListFilm.get(j)));
                if (j < tmpListFilm.size() - 1) {
                    sb.append(separator);
                }
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    /**
     * Uprava polozky seznamu
     *
     * @param cisloPolozky - index puvodni polozky
     * @param upravenaPolozka - nove upravena polozka
     * @return
     */
    public String upravPolozkuHra(int cisloPolozky, SeznamHer upravenaPolozka) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListHra = SeznamHer.seznamAtributu();
        for (int i = 0; i < tmpListHra.size(); i++) {
            if (!upravenaPolozka.prectiAtributy(tmpListHra.get(i)).trim().isEmpty()) {
                seznamHer.get(cisloPolozky).nastavAtributy(tmpListHra.get(i), upravenaPolozka.prectiAtributy(tmpListHra.get(i)));

            }
            sb.append(seznamHer.get(cisloPolozky).prectiAtributy(tmpListHra.get(i))).append(" ");
        }
        return sb.toString();
    }

    /**
     * Uprava polozky seznamu
     *
     * @param cisloPolozky - index puvodni polozky
     * @param upravenaPolozka - nove upravena polozka
     * @return
     */
    public String upravPolozkuFilm(int cisloPolozky, SeznamFilmu upravenaPolozka) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListFilm = SeznamFilmu.seznamAtributu();
        for (int i = 0; i < tmpListFilm.size(); i++) {
            if (!upravenaPolozka.prectiAtributy(tmpListFilm.get(i)).trim().isEmpty()) {
                seznamFilmu.get(cisloPolozky).nastavAtributy(tmpListFilm.get(i), upravenaPolozka.prectiAtributy(tmpListFilm.get(i)));
            }
            sb.append(seznamFilmu.get(cisloPolozky).prectiAtributy(tmpListFilm.get(i))).append(" ");
        }
        return sb.toString();
    }

    /**
     * Uprava polozky seznamu
     *
     * @param cisloPolozky - index puvodni polozky
     * @param upravenaPolozka - nove upravena polozka
     * @return
     */
    public String upravPolozkuKniha(int cisloPolozky, SeznamKnih upravenaPolozka) {
        StringBuilder sb = new StringBuilder();
        ArrayList<String> tmpListKniha = SeznamKnih.seznamAtributu();
        for (int i = 0; i < tmpListKniha.size(); i++) {
            if (!upravenaPolozka.prectiAtributy(tmpListKniha.get(i)).trim().isEmpty()) {
                seznamKnih.get(cisloPolozky).nastavAtributy(tmpListKniha.get(i), upravenaPolozka.prectiAtributy(tmpListKniha.get(i)));

            }
            sb.append(seznamKnih.get(cisloPolozky).prectiAtributy(tmpListKniha.get(i))).append(" ");
        }
        return sb.toString();
    }

    /**
     * Smaze zadanou polozku seznamu
     *
     * @param cisloPolozky - index polozky ke smazani
     */
    public void smazPolozkuHra(int cisloPolozky) {
        seznamHer.remove(cisloPolozky);
    }

    /**
     * Smaze zadanou polozku seznamu
     *
     * @param cisloPolozky - index polozky ke smazani
     */
    public void smazPolozkuFilm(int cisloPolozky) {
        seznamFilmu.remove(cisloPolozky);
    }

    /**
     * Smaze zadanou polozku seznamu
     *
     * @param cisloPolozky - index polozky ke smazani
     */
    public void smazPolozkuKniha(int cisloPolozky) {
        seznamKnih.remove(cisloPolozky);
    }

    /**
     * Ulozi aktualni seznam do souboru ve formatu csv
     *
     * @param jmSoub - nazev souboru
     * @throws IOException
     */
    public void ulozSeznamHry(String jmSoub) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jmSoub)))) {
            bw.append(hryHeader).append("\n");
            bw.append(getVypisHry(separator, false));
        }
    }

    /**
     * Ulozi aktualni seznam do souboru ve formatu csv
     *
     * @param jmSoub - nazev souboru
     * @throws IOException
     */
    public void ulozSeznamFilmy(String jmSoub) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jmSoub)))) {
            bw.append(filmyHeader).append("\n");
            bw.append(getVypisFilmy(separator, false));
        }
    }

    /**
     * Ulozi aktualni seznam do souboru ve formatu csv
     *
     * @param jmSoub - nazev souboru
     * @throws IOException
     */
    public void ulozSeznamKnihy(String jmSoub) throws IOException {
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(jmSoub)))) {
            bw.append(knihyHeader).append("\n");
            bw.append(getVypisKnihy(separator, false));
        }
    }

    /**
     * Seradi seznam podle volby uzivatele
     *
     * @param volba - volba razeni
     * @param reverse - smer razeni
     */
    public void seradSeznamHry(int volba, boolean reverse) {
        SeznamHer.volbaTrideni = volba;
        if (reverse == true) {
            Collections.sort(seznamHer, Collections.reverseOrder());
        } else {
            Collections.sort(seznamHer);
        }
    }

    /**
     * Seradi seznam podle volby uzivatele
     *
     * @param volba - volba razeni
     * @param reverse - smer razeni
     */
    public void seradSeznamFilmy(int volba, boolean reverse) {
        SeznamFilmu.volbaTrideni = volba;
        if (reverse == true) {
            Collections.sort(seznamFilmu, Collections.reverseOrder());
        } else {
            Collections.sort(seznamFilmu);
        }
    }

    /**
     * Seradi seznam podle volby uzivatele
     *
     * @param volba - volba razeni
     * @param reverse - smer razeni
     */
    public void seradSeznamKnihy(int volba, boolean reverse) {
        SeznamKnih.volbaTrideni = volba;

        if (reverse == true) {
            Collections.sort(seznamKnih, Collections.reverseOrder());
        } else {
            Collections.sort(seznamKnih);
        }
    }

    /*private String diakritika(String slovo) {
        String[] diakritik = new String[]{
            "Á", "É", "Í", "Ó", "Ú", "Ý", "Č", "Ď", "Ě", "Ň", "Ř", "Š", "Ť", "Ž", "Ů",
            "á", "é", "í", "ó", "ú", "ý", "č", "ď", "ě", "ň", "ř", "š", "ť", "ž", "ů"};

        String[] bezdiakritik = new String[]{
            "A", "E", "I", "O", "U", "Y", "C", "D", "E", "N", "R", "S", "T", "Z", "U",
            "a", "e", "i", "o", "u", "y", "c", "d", "e", "n", "z", "s", "t", "z", "u"};
        for (int i = 0; i < diakritik.length; i++) {
            if (slovo.contains(diakritik[i])) {
                System.out.println(diakritik[i]);
                slovo = slovo.replace(diakritik[i], bezdiakritik[i]);
            }
        }
        return slovo;
    }*/

    /*public static int stringCompare(String str1, String str2) 
     { 
  
     int l1 = str1.length(); 
     int l2 = str2.length(); 
     int lmin = Math.min(l1, l2); 
  
     for (int i = 0; i < lmin; i++) { 
     int str1_ch = (int)str1.charAt(i); 
     int str2_ch = (int)str2.charAt(i); 
  
     if (str1_ch != str2_ch) { 
     return str1_ch - str2_ch; 
     } 
     } 
  
     // Edge case for strings like 
     // String 1="Geeks" and String 2="Geeksforgeeks" 
     if (l1 != l2) { 
     return l1 - l2; 
     } 
  
     // If none of the above conditions is true, 
     // it implies both the strings are equal 
     else { 
     return 0; 
     } 
     }*/
}
