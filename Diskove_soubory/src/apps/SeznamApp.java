package apps;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Scanner;

import tools.Seznam;
import tools.SeznamKnih;

/**
 *
 * @author hanzlik
 */

public class SeznamApp {

    private static final Scanner sc = new Scanner(System.in,"Cp1250");
    public static ResourceBundle rb = ResourceBundle.getBundle(
            "popisky.SeznamPopisky");
    public static ResourceBundle rb_cz = ResourceBundle.getBundle(
            "popisky.SeznamPopisky", new Locale("cs", "CZ"));
    public static ResourceBundle rb_en = ResourceBundle.getBundle(
            "popisky.SeznamPopisky", Locale.US);

    /*private static String dataAdr = "data";
     private static String jmSouborHry = dataAdr + File.separator + "Hry.csv";
     private static String jmSouborKnihy = dataAdr + File.separator + "Knihy.csv";
     private static String jmSouborFilmy= dataAdr + File.separator + "Filmy.csv";*/
    private static String dataAdr;
    private static String jmSouborHry;
    private static String jmSouborKnihy;
    private static String jmSouborFilmy;

    private static tools.Seznam instance;

    public static void main(String[] args) throws IOException {
        // hlavni ridici smycka programu
        System.out.println("1. Cestina");
        System.out.println("2. English");
        switch(nacistVolbu()){
            case 1:
                rb = rb_cz;
                break;
            case 2:  
                rb = rb_en;
                break;
            default:
                System.out.println("Chybna volba, nastaven cesky jazyk");
        }
        
        System.out.println(rb.getString("makeDir"));
        System.out.println(rb.getString("helpMakeDirAbsolute")+rb.getString("exampleDirAbsolute"));
        System.out.println(rb.getString("helpMakeDirRelative")+rb.getString("exampleDirRelative"));
        File file;
        do {
            dataAdr = sc.nextLine();
            file = new File(dataAdr);
            if (!file.exists()) {
                
                if (file.mkdir()) {
                    System.out.println(rb.getString("newDir"));
                } else {
                    System.out.println(rb.getString("errorDir"));
                }
            }  else {
                System.out.println(rb.getString("existsDir"));
            }       
        } while(!file.exists());
        System.out.println(rb.getString("pathDir"));
        System.out.println(file.getAbsolutePath());
        
        jmSouborHry = dataAdr + File.separator + "Hry.csv";
        jmSouborKnihy = dataAdr + File.separator + "Knihy.csv";
        jmSouborFilmy = dataAdr + File.separator + "Filmy.csv";

        instance = tools.Seznam.getInstance(jmSouborHry, jmSouborKnihy, jmSouborFilmy);

        boolean konecProgramu = false;
        while (!konecProgramu) {
            // vypsat menu
            vypisHlavniMenuProgramu();
            // nacist volbu uzivatel
            int volbaUzivatele = nacistVolbu();
            // provest akci pozadovanou uzivatelem
            switch (volbaUzivatele) {
                case 0:
                    konecProgramu = true;
                    break;
                case 1:
                    seznamZapis();
                    break;
                case 2:
                    seznamVypis();
                    break;
                case 3:
                    seznamUprava();
                    break;
                case 4:
                    seznamUlozeni();
                    break;
                case 5:
                    seznamSmazani();
                    break;
                case 6:
                    seznamZmenaRazeni();
                    break;
                case 11:
                    rb = rb_cz;
                    break;
                case 12:
                    rb = rb_en;
                    break;
                default:
                    System.out.println(rb.getString("Err1"));
            }
        }
    }

    private static void vypisHlavniMenuProgramu() {
        System.out.println("");
        System.out.println(rb.getString("MMenuTitle"));
        System.out.println(rb.getString("MMItem1"));
        System.out.println(rb.getString("MMItem2"));
        System.out.println(rb.getString("MMItem3"));
        System.out.println(rb.getString("MMItem4"));
        System.out.println(rb.getString("MMItem5"));
        System.out.println(rb.getString("MMItem6"));
        System.out.println(rb.getString("MMItem11"));
        System.out.println(rb.getString("MMItem12"));
        System.out.println(rb.getString("MMItem0"));
    }

    private static int nacistVolbu() {
        int volba = -1;
        System.out.println(rb.getString("Prompt"));
        try {
            volba = sc.nextInt();
        } catch (InputMismatchException e) {
            // neplatna volba
            volba = -1;
        } finally {
            sc.nextLine();
        }
        return volba;
    }

    private static void seznamy() {
        System.out.println("");
        System.out.println(rb.getString("ListChoice"));
        System.out.println(rb.getString("List1"));
        System.out.println(rb.getString("List2"));
        System.out.println(rb.getString("List3"));
    }

    private static void seznamZapis() {

        seznamy();
        try {
            switch (nacistVolbu()) {
                case 1://hry
                    System.out.println(rb.getString("addItems"));
                    System.out.println();
                    tools.SeznamHer tmpHra = new tools.SeznamHer(null, null, 0, null);
                    ArrayList<String> tmpListHra = tmpHra.seznamAtributu();
                    for (int i = 0; i < tmpListHra.size(); i++) {
                        System.out.println(rb.getString(tmpListHra.get(i)));
                        tmpHra.nastavAtributy(tmpListHra.get(i), sc.nextLine());
                    }
                    instance.pridejDoSeznamuHer(tmpHra);
                    break;

                case 2://filmy
                    System.out.println(rb.getString("addItems"));
                    System.out.println();
                    tools.SeznamFilmu tmpFilm = new tools.SeznamFilmu(null, null, 0, null);
                    ArrayList<String> tmpListFilm = tmpFilm.seznamAtributu();
                    for (int i = 0; i < tmpListFilm.size(); i++) {
                        System.out.println(rb.getString(tmpListFilm.get(i)));
                        tmpFilm.nastavAtributy(tmpListFilm.get(i), sc.nextLine());
                    }
                    instance.pridejDoSeznamuFilmu(tmpFilm);
                    break;

                case 3://knihy
                    System.out.println(rb.getString("addItems"));
                    System.out.println();

                    tools.SeznamKnih tmpKniha = new tools.SeznamKnih(null, null, 0, null, null);
                    ArrayList<String> tmpListKniha = tmpKniha.seznamAtributu();
                    for (int i = 0; i < tmpListKniha.size(); i++) {
                        System.out.println(rb.getString(tmpListKniha.get(i)));
                        //System.out.println(tmpListKniha.get(i));
                        tmpKniha.nastavAtributy(tmpListKniha.get(i), sc.nextLine());
                    }
                    instance.pridejDoSeznamuKnih(tmpKniha);
                    break;

                default:
                    System.out.println(rb.getString("Err1"));
            }
        } catch (NumberFormatException e) {
            System.out.println(rb.getString("errorNumber"));
        }
    }

    private static void seznamVypis() {
        seznamy();
        int volba = nacistVolbu();
        switch (volba) {
            case 1:
                System.out.println("Hry");
                System.out.println(instance.getVypisHry(" ", true));
                break;
            case 2:
                System.out.println("Filmy");
                System.out.println(instance.getVypisFilmy(" ", true));
                break;
            case 3:
                System.out.println("Knihy");
                System.out.println(instance.getVypisKnihy(" ", true));
                break;
            default:
                System.out.println(rb.getString("Err1"));
        }
    }

    private static void seznamUprava() {
        seznamy();
        int volba = nacistVolbu();
        int cisloPolozky;
        try {
            switch (volba) {
                case 1:
                    System.out.println(instance.getVypisHry(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    tools.SeznamHer tmpHra = new tools.SeznamHer(null, null, 0, null);
                    ArrayList<String> tmpListHra = tmpHra.seznamAtributu();
                    System.out.println(rb.getString("infoModify"));
                    for (int i = 0; i < tmpListHra.size(); i++) {
                        System.out.println(rb.getString(tmpListHra.get(i)));
                        tmpHra.nastavAtributy(tmpListHra.get(i), sc.nextLine());
                    }
                    System.out.println(rb.getString("outputModify"));
                    System.out.println(instance.upravPolozkuHra(cisloPolozky, tmpHra));
                    break;

                case 2:
                    System.out.println(instance.getVypisFilmy(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    tools.SeznamFilmu tmpFilm = new tools.SeznamFilmu(null, null, 0, null);
                    ArrayList<String> tmpListFilm = tmpFilm.seznamAtributu();
                    System.out.println(rb.getString("infoModify"));
                    for (int i = 0; i < tmpListFilm.size(); i++) {
                        System.out.println(rb.getString(tmpListFilm.get(i)));
                        tmpFilm.nastavAtributy(tmpListFilm.get(i), sc.nextLine());
                    }
                    System.out.println(rb.getString("outputModify"));
                    System.out.println(instance.upravPolozkuFilm(cisloPolozky, tmpFilm));
                    break;

                case 3:
                    System.out.println(instance.getVypisKnihy(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    tools.SeznamKnih tmpKniha = new tools.SeznamKnih(null, null, 0, null, null);
                    ArrayList<String> tmpListKniha = tmpKniha.seznamAtributu();
                    System.out.println(rb.getString("infoModify"));
                    for (int i = 0; i < tmpListKniha.size(); i++) {
                        System.out.println(rb.getString(tmpListKniha.get(i)));
                        tmpKniha.nastavAtributy(tmpListKniha.get(i), sc.nextLine());
                    }
                    System.out.println(rb.getString("outputModify"));
                    System.out.println(instance.upravPolozkuKniha(cisloPolozky, tmpKniha));
                    break;

                default:
                    System.out.println(rb.getString("Err1"));
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println(rb.getString("errorIndex"));
            //System.out.println(e);
        } catch (NumberFormatException e) {
            System.out.println(rb.getString("errorNumber"));
        }
    }

    private static void seznamUlozeni() throws IOException {
        seznamy();
        int volba = nacistVolbu();
        switch (volba) {
            case 1:

                instance.ulozSeznamHry(jmSouborHry);
                break;
            case 2:
                instance.ulozSeznamFilmy(jmSouborFilmy);
                break;
            case 3:
                instance.ulozSeznamKnihy(jmSouborKnihy);
                break;
            default:
                System.out.println(rb.getString("Err1"));
        }
    }

    private static void seznamSmazani() {
        seznamy();
        int cisloPolozky;
        try {
            switch (nacistVolbu()) {
                case 1:
                    System.out.println(instance.getVypisHry(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    instance.smazPolozkuHra(cisloPolozky);
                    break;
                case 2:
                    System.out.println(instance.getVypisFilmy(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    instance.smazPolozkuFilm(cisloPolozky);
                    break;
                case 3:
                    System.out.println(instance.getVypisKnihy(" ", true));
                    System.out.println(rb.getString("chooseItem"));
                    cisloPolozky = nacistVolbu();
                    instance.smazPolozkuKniha(cisloPolozky);
                    break;
                default:
                    System.out.println(rb.getString("Err1"));
            }
        } catch (IndexOutOfBoundsException e) {
            System.out.println(rb.getString("errorIndex"));
        }
    }

    private static void seznamZmenaRazeni() {
        boolean reverse = false;
        seznamy();
        int volbaSeznamu = nacistVolbu();

        System.out.println(rb.getString("sortBy"));
        System.out.println("1. " + rb.getString("nazev"));
        System.out.println("2. " + rb.getString("rok_vydani"));
        int volbaTrideni = nacistVolbu();
        if (volbaTrideni != 1 && volbaTrideni != 2) {
            System.out.println(rb.getString("Err1"));
            return;
        }
        System.out.println(rb.getString("direction"));
        System.out.println("1. " + rb.getString("ascending"));
        System.out.println("2. " + rb.getString("descending"));
        int volbaSmeru = nacistVolbu();
        if (volbaSmeru != 1 && volbaSmeru != 2) {
            System.out.println(rb.getString("Err1"));
            return;
        }
        if (volbaSmeru == 1) {
            reverse = true;
        }
        switch (volbaSeznamu) {
            case 1:
                instance.seradSeznamHry(volbaTrideni, reverse);
                break;
            case 2:
                instance.seradSeznamFilmy(volbaTrideni, reverse);
                break;
            case 3:
                instance.seradSeznamKnihy(volbaTrideni, reverse);
                break;
            default:
                System.out.println(rb.getString("Err1"));
        }
    }
}
