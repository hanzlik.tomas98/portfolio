﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Xml;
using Path = System.IO.Path;
using Nest;

namespace SPVKP_PRJ
{
    /// <summary>
    /// Okno zobrazené po stisknutí tlačítka pro vyhledávání.
    /// Vstupem jsou buď data z okna ElasticSearch, nebo z hlavního okna 
    /// </summary>
    public partial class Window1 : Window
    {
        List<ListPraci> listPraci = new List<ListPraci>();
        List<ListVysledku> listVysledku = new List<ListVysledku>();
        List<ListSouboru> listSouboru = new List<ListSouboru>();
        IReadOnlyCollection<WindowElasticSearch.ResultPapers> resDoc;
        bool setPDF = false;
        bool isElastic = false;
        string filtrAuthors, filtrAbstract, filtrTitle;
        string fileXML, folderPDF;
        string patternAll;
        Regex regAll;
        //public ElasticClient esClient;
        MainWindow instance;

        /// <summary>
        /// Nastavení okna podle vstupu.
        /// V tomto případě je v stup z okna ElasticSearch.
        /// v searchResponse je výsledek z hledání ElasticSearch.
        /// </summary>
        public Window1(ISearchResponse<WindowElasticSearch.ResultPapers> searchResponse, WindowElasticSearch instance, string selectedFile)
        {
            isElastic = true;
            InitializeComponent();
            labelSearchedAuthors.Content = instance.textBoxAuthors.Text;
            labelSearchedKeywords.Content = instance.textBoxAbstract.Text.ToString();
            labelSearchedTitle.Content = instance.textBoxTitle.Text.ToString();
            resDoc = searchResponse.Documents;

            if (selectedFile != null && selectedFile != "")
            {
                folderPDF = selectedFile;
                setPDF = true;
            }
            foreach (var item in resDoc)
            {

                listBoxResults.Items.Add(item.title);
                listVysledku.Add(new ListVysledku(item.title, item.authors.ToString(), item.file, item.@abstract,0,0,0));
            }
            filtrAuthors = labelSearchedAuthors.Content.ToString();
            filtrAbstract = labelSearchedKeywords.Content.ToString();
            filtrTitle = labelSearchedTitle.Content.ToString();
            SetRegex(filtrTitle,filtrAuthors,filtrAbstract);
        }
        /// <summary>
        /// Nastavení okna podle vstupu.
        /// V tomto případě je vstup z klasického vyhledávání.
        /// </summary>
        public Window1(string selectedFile, MainWindow instance)
        {
            
            this.instance = instance;
            InitializeComponent();
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            string[] tempSelectedFile = selectedFile.Split(';');
            fileXML = tempSelectedFile[0];
            folderPDF = tempSelectedFile[1]; 
            if (folderPDF != null && folderPDF != "") 
            {
                setPDF = true;
            }

            labelSearchedAuthors.Content = instance.textBoxAuthors.Text.ToString();
            labelSearchedKeywords.Content = instance.textBoxAbstract.Text.ToString();
            labelSearchedTitle.Content = instance.textBoxTitle.Text.ToString();
            filtrAuthors = labelSearchedAuthors.Content.ToString();
            filtrAbstract = labelSearchedKeywords.Content.ToString();
            filtrTitle = labelSearchedTitle.Content.ToString();

            XmlReader xmlReader = XmlReader.Create(fileXML);
            ReadXMLFile(xmlReader, listPraci);
            CompareData(filtrAuthors, filtrAbstract, filtrTitle, listPraci, listVysledku);
            listVysledku.Sort((x, y) => y.getCelkovaRelevance().CompareTo(x.getCelkovaRelevance()));

            //ukonceni mereni rychlosti vyhledavani 
            //watch.Stop();
            //var elapsedMs = watch.ElapsedMilliseconds;
            //MessageBox.Show("Search time: " + elapsedMs + " ms", "Time elapsed", MessageBoxButton.OK, MessageBoxImage.Information);

            string paperTitle;
            int indexOfRemove;
            foreach (var paperVysledek in listVysledku)
            {
                indexOfRemove = paperVysledek.nazevPrace.IndexOf("—");
                if (indexOfRemove>0)
                {
                    paperTitle = paperVysledek.nazevPrace.Remove(0, indexOfRemove + 2);
                    listBoxResults.Items.Add(paperVysledek.getCelkovaRelevance() + "  " + paperTitle);
                }
                else
                {
                    listBoxResults.Items.Add(paperVysledek.getCelkovaRelevance() + "  " + paperVysledek.nazevPrace);
                }
            }
            
            SetRegex(filtrTitle, filtrAuthors, filtrAbstract);
        }

        /// <summary>
        /// Pokud je nastavena složka s publikacemi v PDF,
        /// tak je stiskem tlačítka otevřena konkrétní publikace.
        /// </summary>
        private void BtnOpenPDF_Click(object sender, RoutedEventArgs e)
        {
            
            try
            {
                int index = listBoxResults.SelectedIndex;
                string selectedPathPDF = Path.Combine(folderPDF, Path.GetFileName(listVysledku[index].cestaPDF));
                System.Diagnostics.Process.Start(selectedPathPDF);
            }
            catch (ArgumentNullException ex)
            {
                ErrorBox(ex);
            }
            catch (System.ComponentModel.Win32Exception ex)
            {
                ErrorBox(ex);
            }
        }
        /// <summary>
        /// metoda pro zobrazení chybové hlášky
        /// </summary>
        private void ErrorBox(Exception ex)
        {
            MessageBox.Show("Error: " + ex, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            MessageBox.Show("Try to set the path to PDF folder again!", "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
        }

        /// <summary>
        /// Nastavení regulárního výrazu pro zvýrazenění slov v richTextBoxu.
        /// </summary>
        private void SetRegex(string filtrTitle,string filtrAuthors,string filtrAbstract)
        {
            if (filtrAuthors != "" && filtrAbstract != "" && filtrTitle != "")
            {
                patternAll = CreateRegexPattern(filtrAuthors + "," + filtrAbstract + "," + filtrTitle);
            }
            else
            {
                if (filtrAuthors != "")
                {
                    if (filtrAbstract != "")
                    {
                        patternAll = CreateRegexPattern(filtrAuthors + "," + filtrAbstract);
                    }
                    else if (filtrTitle != "")
                    {
                        patternAll = CreateRegexPattern(filtrAuthors + "," + filtrTitle);
                    }
                    else
                    {
                        patternAll = CreateRegexPattern(filtrAuthors);
                    }
                }
                else
                {
                    if (filtrAbstract != "")
                    {
                        if (filtrTitle != "")
                        {
                            patternAll = CreateRegexPattern(filtrAbstract + "," + filtrTitle);
                        }
                        else
                        {
                            patternAll = CreateRegexPattern(filtrAbstract);
                        }
                    }
                    else
                    {
                        patternAll = CreateRegexPattern(filtrTitle);
                    }
                }
            }
            if (!isElastic)
            {
                if (instance.checkBoxCase.IsChecked.Value)
                {
                    regAll = new Regex(patternAll, RegexOptions.Compiled);
                }
                else
                {
                    regAll = new Regex(patternAll, RegexOptions.Compiled | RegexOptions.IgnoreCase);
                }
            }
            else
            {
                regAll = new Regex(patternAll, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            }
        }

        /// <summary>
        /// Uložení výsledku vyhledávání v plain textu
        /// </summary>
        private void BtnSaveResults_Click(object sender, RoutedEventArgs e)
        {
           
            SaveFileDialog saveFileDialog1 = new SaveFileDialog();
            saveFileDialog1.Filter = "text file|*.txt";
            saveFileDialog1.Title = "Save the search result";
            saveFileDialog1.ShowDialog();

            if (saveFileDialog1.FileName != "")
            {
                //true -> append textu pokud soubor existuje
                System.IO.StreamWriter saveFile = new System.IO.StreamWriter(saveFileDialog1.FileName, true);

                saveFile.WriteLine("-------------------------");
                saveFile.WriteLine("Search settings");
                saveFile.WriteLine("Title: " + filtrTitle);
                saveFile.WriteLine("Authors: " + filtrAuthors);
                saveFile.WriteLine("Abstract: " + filtrAbstract);
                saveFile.WriteLine("\nResults:");
                foreach (var item in listVysledku)
                {
                    saveFile.WriteLine(item.nazevPrace + " |file: " + item.cestaPDF);
                }
                saveFile.WriteLine("-----------end-----------");
                saveFile.Close();
            }
        }

        /// <summary>
        /// Metoda která při změně vybrané publikace v listboxu výsledků 
        /// zobrazí konkrétní informace v textboxu pro detail práce.
        /// </summary>
        private void ListBoxResults_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (setPDF)
            {
                if(listVysledku[listBoxResults.SelectedIndex].cestaPDF != null && listVysledku[listBoxResults.SelectedIndex].cestaPDF != "")
                {
                    btnOpenPDF.IsEnabled = true;
                }
            }
            try
            {
                richTextBoxDetailed.IsEnabled = true;
                string resultInfoText = null;
                if (resDoc != null)
                {
                    resultInfoText = "Name: " + resDoc.ElementAt(listBoxResults.SelectedIndex).title + "\n\n";
                    resultInfoText += "Authors: ";
                    foreach (var item in resDoc.ElementAt(listBoxResults.SelectedIndex).authors)
                    {
                        
                        if (item != resDoc.ElementAt(listBoxResults.SelectedIndex).authors.Last())
                        {
                            resultInfoText += item + ",";
                        }
                        else
                        {
                            resultInfoText += item;
                        }
                    }
                    resultInfoText += "\n\n";
                    resultInfoText += "Abstract: " + resDoc.ElementAt(listBoxResults.SelectedIndex).@abstract + "\n\n";
                    resultInfoText += "File: " + resDoc.ElementAt(listBoxResults.SelectedIndex).file + "\n\n";
                }
                else
                {
                    resultInfoText = "Name: " + listVysledku[listBoxResults.SelectedIndex].nazevPrace + "\n\n";
                    resultInfoText += "Authors: " + listVysledku[listBoxResults.SelectedIndex].autori + "\n\n";
                    resultInfoText += "Abstract: " + listVysledku[listBoxResults.SelectedIndex].abstrakt + "\n\n";
                    resultInfoText += "File: " + listVysledku[listBoxResults.SelectedIndex].cestaPDF;
                }
                
                richTextBoxDetailed.Document.Blocks.Clear();
                richTextBoxDetailed.Document.Blocks.Add(new Paragraph(new Run(resultInfoText)));
            }
            catch (ArgumentOutOfRangeException)
            {
                richTextBoxDetailed.IsEnabled = false;
            }
            //pokud bych chtel dve barvy tak nutno najit text pointery pro authors & abstract a nastavit spravne range
            //string patternAuthor, patternKeyWord;
            //patternAuthor = SetRegex(filtrAuthors);
            //patternKeyWord = SetRegex(filtrKeyWords);
            //Regex regAuhtor = new Regex(patternAuthor, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            //Regex regKeyWord = new Regex(patternKeyWord, RegexOptions.Compiled | RegexOptions.IgnoreCase);
            
            var start = richTextBoxDetailed.Document.ContentStart;
            while (start != null && start.CompareTo(richTextBoxDetailed.Document.ContentEnd) < 0)
            {
                if (start.GetPointerContext(LogicalDirection.Forward) == TextPointerContext.Text)
                {
                    var matchAll = regAll.Match(start.GetTextInRun(LogicalDirection.Forward));
                    var textrange = new TextRange(start.GetPositionAtOffset(matchAll.Index, LogicalDirection.Forward), start.GetPositionAtOffset(matchAll.Index + matchAll.Length, LogicalDirection.Backward));
                    textrange.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(Colors.Blue));
                    textrange.ApplyPropertyValue(TextElement.FontWeightProperty, FontWeights.Bold);
                    start = textrange.End;
                }
                start = start.GetNextContextPosition(LogicalDirection.Forward);
            }
        }
        /// <summary>
        /// Vytvoření správného tvaru regulárního výrazu
        /// </summary>
        private string CreateRegexPattern(string stringForRegex)
        {
            string[] stringArray;
            stringArray = stringForRegex.Split(',');

            string regexPattern = "(";
            foreach (var item in stringArray)
            {
                if (item.Equals(stringArray.Last()))
                {
                    regexPattern = regexPattern + item.Trim();
                }
                else
                {
                    regexPattern = regexPattern + item.Trim() + "|";
                }
            }
            regexPattern = regexPattern + ")";

            return regexPattern;
        }

        /// <summary>
        /// Přečtení dat z XML a zápis do listu s publikacemi
        /// </summary>
        private void ReadXMLFile(XmlReader xmlReader, List<ListPraci> listPraci)
        {
            string authors, paperAbstract, path, title;
            xmlReader.ReadToFollowing("paper");
            do
            {
                xmlReader.ReadToFollowing("title");
                title = xmlReader.ReadElementContentAsString();
                xmlReader.ReadToFollowing("author");
                authors = xmlReader.ReadElementContentAsString();
                while (xmlReader.ReadToNextSibling("author"))
                {
                    authors = authors + "," + xmlReader.ReadElementContentAsString();
                }

                xmlReader.ReadToFollowing("abstract");
                paperAbstract = xmlReader.ReadElementContentAsString();
                xmlReader.ReadToFollowing("file_path");
                path = xmlReader.ReadElementContentAsString();
                listPraci.Add(new ListPraci(title, authors, path, paperAbstract));
            } while (xmlReader.ReadToFollowing("paper"));

        }
        /// <summary>
        /// porovnání dat s filtrem a zápis do listu výsledků
        /// </summary>
        private void CompareData(string filtrAuthors, string filtrAbstract, string filtrTitle, List<ListPraci> listPraci, List<ListVysledku> listVysledku)
        {
            string[] poleFiltrAuthors;
            string[] poleFiltrAbstract;
            string[] poleFiltrTitle;
            poleFiltrAuthors = filtrAuthors.Split(',');
            poleFiltrAbstract = filtrAbstract.Split(',');
            poleFiltrTitle = filtrTitle.Split(',');

            int maxRelevance = poleFiltrAuthors.Count() + poleFiltrAbstract.Count() + poleFiltrTitle.Count();
            if (filtrAbstract.Trim().Equals(""))
            {
                maxRelevance--;
            }
            if (filtrAuthors.Trim().Equals(""))
            {
                maxRelevance--;
            }
            if (filtrTitle.Trim().Equals(""))
            {
                maxRelevance--;
            }
            int relevanceAuthors;
            int relevanceTitle;
            int relevanceAbstract;
            foreach (ListPraci paper in listPraci)
            {
                relevanceAuthors = 0;
                relevanceAbstract = 0;
                relevanceTitle = 0;

                relevanceAuthors = relevanceCounter(paper.autori, filtrAuthors, poleFiltrAuthors);
                relevanceAbstract = relevanceCounter(paper.abstrakt, filtrAbstract, poleFiltrAbstract);
                relevanceTitle = relevanceCounter(paper.nazevPrace, filtrTitle, poleFiltrTitle);
                
                if (instance.radioBtnAbsolute.IsChecked.Value)
                {
                    if ((relevanceAuthors + relevanceAbstract + relevanceTitle) == maxRelevance)
                    {
                        listVysledku.Add(new ListVysledku(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, relevanceAuthors, relevanceAbstract, relevanceTitle));
                    }
                }
                else if (instance.radioBtnOr.IsChecked.Value)
                {
                    if (relevanceAuthors > 0 || relevanceAbstract > 0 || relevanceTitle > 0)
                    {
                        listVysledku.Add(new ListVysledku(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, relevanceAuthors, relevanceAbstract, relevanceTitle));
                    }
                }
                else if (instance.radioBtnAnd.IsChecked.Value)
                {
                    if ((relevanceAuthors > 0 || filtrAuthors.Trim().Equals("")) &&
                        (relevanceAbstract > 0 || filtrAbstract.Trim().Equals("")) &&
                        (relevanceTitle > 0 || filtrTitle.Trim().Equals("")))
                    {
                        listVysledku.Add(new ListVysledku(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, relevanceAuthors, relevanceAbstract, relevanceTitle));
                    }
                }

            }
        }
        /// <summary>
        /// Výpočet hodnoty relevance pro konkrétní publikaci
        /// </summary>
        private int relevanceCounter(string paper, string filtr, string[] poleFiltr)
        {
            int relevanceCounter = 0;

            if (!filtr.Equals(""))
            {
                foreach (string filtered in poleFiltr)
                {
                    if (instance.checkBoxCase.IsChecked.Value)
                    {
                        if (instance.checkBoxExactWords.IsChecked.Value)
                        {
                            bool match1 = Regex.IsMatch(paper, $@"\b{filtered}\b");
                            if (match1)
                            {
                                relevanceCounter++;
                            }
                        }
                        else
                        {
                            if (paper.Contains(filtered))
                            {
                                relevanceCounter++;
                            }
                        }
                    }
                    else 
                    {
                        if (instance.checkBoxExactWords.IsChecked.Value)
                        {
                            bool match2 = Regex.IsMatch(paper, $@"\b{filtered}\b", RegexOptions.IgnoreCase);
                            if (match2)
                            {
                                relevanceCounter++;
                            }
                        }
                        else
                        {
                            if (paper.ToLowerInvariant().Contains(filtered.ToLowerInvariant()))
                            {
                                relevanceCounter++;
                            }
                        }
                    }
                }
            }
            return relevanceCounter;
        }
    }
}
