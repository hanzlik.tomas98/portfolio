﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Nest;
using Microsoft.Win32;

namespace SPVKP_PRJ
{
    /// <summary>
    /// Okno zobrazené po kliknutí na tlačítko elastic search.
    /// Po navázání spojení se ukážou nalezené indexy (dokumenty).
    /// V těch se pak dá vyhledávat podle klíčových slov.
    /// Stiskem tlačítka search se zobrazí okno s výsledky vyhledávání.
    /// Elastic search je search engine a zároveň funguje jako noSQL databáze
    /// </summary>
    public partial class WindowElasticSearch : Window
    {
        ElasticClient esClient;
        WindowElasticSearch instance;
        List<ListSouboru> listSouboru = new List<ListSouboru>();

        /// <summary>
        /// Třída pro vytvoření objektu s náležitostmi pro vyhledávání v indexech ElasticSearch
        /// </summary>
        public class ResultPapers
        {
            [JsonProperty(PropertyName = "title")]
            public string title { get; set; }

            [JsonProperty(PropertyName = "authors")]
            public string[] authors { get; set; }

            [JsonProperty(PropertyName = "abstract")]
            public string @abstract { get; set; }
            [JsonProperty(PropertyName = "file")]
            public string file { get; set; }
        }

        /// <summary>
        /// Nastavení spojení a zobrazení okna
        /// </summary>
        public WindowElasticSearch()
        {
            var settings = new ConnectionSettings(new Uri("http://localhost:9200/"))
               .DefaultIndex("papers")
               .EnableDebugMode()
               .DefaultFieldNameInferrer(s => s) //disable CAMEL CASE
               .PrettyJson();
            esClient = new ElasticClient(settings);
            var esIndexes = esClient.Indices.GetAsync(new GetIndexRequest(Indices.All));
            var listIndexes = esIndexes.Result.Indices.Keys.ToList();
            listIndexes.RemoveAll(item => item.ToString().StartsWith("."));

            InitializeComponent();

            instance = this;
            foreach (var item in listIndexes)
            {
                listBoxIndexy.Items.Add(item.ToString());
                listSouboru.Add(new ListSouboru(null, null));
            }
            if (listBoxIndexy.HasItems)
            {
                btnSetPDF.IsEnabled = true;
            }
        }
        /// <summary>
        /// Tlačítko pro vyhledávání za využití ElasticSearch.
        /// Vlastnosti vyhledávání se mění podle nastavení radio button.
        /// </summary>
        private void ButtonSearch(object sender, RoutedEventArgs e)
        {
            //var watch = System.Diagnostics.Stopwatch.StartNew();
            if (radioBtnAnd.IsChecked.Value)
            {
                var searchResponse = esClient.Search<ResultPapers>(s => s.Size(1000)
                    .Index(listBoxIndexy.SelectedItem.ToString())
                    //.AllIndices()
                    .Query(q => q
                         .Match(m => m
                            .Field(f => f.title)
                            .Query(textBoxTitle.Text)
                         ) && q
                         .Match(m => m
                            .Field(f => f.authors)
                            .Query(textBoxAuthors.Text)
                        ) && q
                        .Match(m => m
                            .Field(f => f.@abstract) //"@" -> disable abstract keyword
                            .Query(textBoxAbstract.Text)
                        )
                    )
                );
                //watch.Stop();
                //var elapsedMs = watch.ElapsedMilliseconds;
                //MessageBox.Show("Search time: " + elapsedMs + " ms", "Time elapsed", MessageBoxButton.OK, MessageBoxImage.Information);
                Window1 newWindow = new Window1(searchResponse, instance, listSouboru[listBoxIndexy.SelectedIndex].pathFolderPDF);
                newWindow.Show();
            }
            else if (radioBtnOr.IsChecked.Value)
            {
                var searchResponse = esClient.Search<ResultPapers>(s => s.Size(1000)
                    .Index(listBoxIndexy.SelectedItem.ToString())
                    //.AllIndices()
                    .Query(q => q
                         .Match(m => m
                            .Field(f => f.title)
                            .Query(textBoxTitle.Text)
                         ) || q
                         .Match(m => m
                            .Field(f => f.authors)
                            .Query(textBoxAuthors.Text)
                        ) || q
                        .Match(m => m
                            .Field(f => f.@abstract) //"@" -> disable abstract keyword
                            .Query(textBoxAbstract.Text)
                        )
                    )
                );
                //watch.Stop();
                //var elapsedMs = watch.ElapsedMilliseconds;
                //MessageBox.Show("Search time: " + elapsedMs, "Time elapsed", MessageBoxButton.OK, MessageBoxImage.Information);
                Window1 newWindow = new Window1(searchResponse, instance, listSouboru[listBoxIndexy.SelectedIndex].pathFolderPDF);
                newWindow.Show();
            }
            else
            {
                var searchResponse = esClient.Search<ResultPapers>(s => s.Size(1000)
                    .Index(listBoxIndexy.SelectedItem.ToString())
                    //.AllIndices()
                    .Query(q => q
                         .MatchPhrase(m => m
                            .Field(f => f.title)
                            .Query(textBoxTitle.Text)
                         ) && q
                         .MatchPhrase(m => m
                            .Field(f => f.authors)
                            .Query(textBoxAuthors.Text)
                        ) && q
                        .MatchPhrase(m => m
                            .Field(f => f.@abstract) //"@" -> disable abstract keyword
                            .Query(textBoxAbstract.Text)
                        )
                    )
                );
                //watch.Stop();
                //var elapsedMs = watch.ElapsedMilliseconds;
                //MessageBox.Show("Search time: " + elapsedMs, "Time elapsed", MessageBoxButton.OK, MessageBoxImage.Information);
                Window1 newWindow = new Window1(searchResponse, instance, listSouboru[listBoxIndexy.SelectedIndex].pathFolderPDF);
                newWindow.Show();
            }
            
            //await _client ? protože než příkaz stáhne data tak to chvíli trvá
            //string searchedText = tBoxSearch.Text;
            //https://stackoverflow.com/questions/16235039/nest-how-to-query-against-multiple-indices-and-handle-different-subclasses-doc


            //Window1 newWindow = new Window1();
            //newWindow.Show();
            //tBoxResult.Text = result;

            /* mutliple matches rozdílných fieldů (abstract a authors)
            searchResponse = _client.Search<Project>(s => s
                .Query(q => q
                    .Match(m => m
                        .Field(f => f.LeadDeveloper.FirstName)
                        .Query("Russ")
                    ) && q
                    .Match(m => m
                        .Field(f => f.LeadDeveloper.LastName)
                        .Query("Cam")
                    ) && +q
                    .DateRange(r => r
                        .Field(f => f.StartedOn)
                        .GreaterThanOrEquals(new DateTime(2017, 01, 01))
                        .LessThan(new DateTime(2018, 01, 01))
                    )
                )
            );*/


            /* MatchPhrase -> pro absolutní search - u jména nutno zadat i příjmení
             var searchResponse = client.Search<User>(s => s
                .Query(q => q
                .MatchPhrase(m => m
                .Field(f => f.Username)
                .Query(username))));
             */
        }

        /// <summary>
        /// Tlačítko k vyčištění vyhledávacích polí
        /// </summary>
        private void ButtonClear(object sender, RoutedEventArgs e)
        {
            btnClear.IsEnabled = false;
            textBoxAuthors.Clear();
            textBoxAbstract.Clear();
            textBoxTitle.Clear();
        }
        /// <summary>
        /// Umožňuje vyhledávání pouze pokud je vybrán nějaký index
        /// </summary>
        private void ListBoxIndexy_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnSearch.IsEnabled = true;
        }

        /// <summary>
        /// Přiřazení složky PDF publikací pro konkrétní ročník
        /// </summary>
        private void ButtonSetPDF(object sender, RoutedEventArgs e)
        {
            string folderPDF;
            folderPDF = ChooseFolder();
            listSouboru[listBoxIndexy.SelectedIndex].pathFolderPDF = folderPDF;
        }

        /// <summary>
        ///  Metoda pro vybrání složky 
        /// </summary>
        private string ChooseFolder()
        {
            string folderPath;
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            // Always default to Folder Selection.
            folderBrowser.FileName = "Folder Selection.";

            if (folderBrowser.ShowDialog() == true)
            {
                folderPath = System.IO.Path.GetDirectoryName(folderBrowser.FileName);
                return folderPath;
            }
            return null;
        }
    }
}
