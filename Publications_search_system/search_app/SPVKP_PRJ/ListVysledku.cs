﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPVKP_PRJ
{
    /// <summary>
    /// Třída pro list ve kterém jsou uloženy výsledky.
    /// Dědí od třídy list prací.
    /// Tu rozšiřuje o vlastnost relevance.
    /// </summary>
    class ListVysledku : ListPraci
    {
        private int celkovaRelevance;
        public ListVysledku(string nazevPrace, string autori, string cestaPDF, string abstrakt, int relevanceAuthors, int relevanceAbstract, int relevanceTitle) 
            : base(nazevPrace,autori,cestaPDF,abstrakt)
        {
            //base -> odkaz na konstruktor předka 
            this.relevanceAuthors = relevanceAuthors;
            this.relevanceAbstract = relevanceAbstract;
            this.relevanceTitle = relevanceTitle;
            celkovaRelevance = relevanceAbstract + relevanceAuthors + relevanceTitle;
        }

        public int relevanceAuthors { get; set; }
        public int relevanceAbstract { get; set; }
        public int relevanceTitle { get; set; }
        public int getCelkovaRelevance() {
            return celkovaRelevance;
        }
    }
}
