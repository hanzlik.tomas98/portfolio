﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPVKP_PRJ
{
    class ListPraci
    {
        /// <summary>
        /// Třída pro zápis dat jednotlivých publikací.
        /// </summary>
        public ListPraci(string nazevPrace, string autori, string cestaPDF, string abstrakt)
        {
            this.nazevPrace = nazevPrace;
            this.autori = autori;
            this.cestaPDF = cestaPDF;
            this.abstrakt = abstrakt;
        }
        public string abstrakt { get; set; }
        public string nazevPrace { get; set; }
        public string autori { get; set; }
        public string cestaPDF { get; set; }
    }
}
