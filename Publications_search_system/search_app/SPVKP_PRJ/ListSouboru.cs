﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SPVKP_PRJ
{
    /// <summary>
    /// Třída pro list ve kterém jsou zapsány cesty k souborům.
    /// </summary>
    class ListSouboru
    {
        public ListSouboru(string pathFileXML, string pathFolderPDF)
        {
            this.pathFileXML = pathFileXML;
            this.pathFolderPDF = pathFolderPDF;
        }
        public string pathFileXML { get; set; }
        public string pathFolderPDF { get; set; }
    }
}
