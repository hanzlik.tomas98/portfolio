﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Windows;
using Path = System.IO.Path;
using Nest;

namespace SPVKP_PRJ
{
    /// <summary>
    /// Hlavní vyhledávací okno zobrazené ihned po spuštění.
    /// Umožňuje otevření dalších dvou oken 
    /// a persistentní nastavení výchozí složky pro výběr souborů
    /// </summary>
    public partial class MainWindow : Window
    {
        List<ListSouboru> listSouboru = new List<ListSouboru>();
        MainWindow instance;
        ElasticClient esClient;

        /// <summary>
        /// Zobrazení okna. Pokud je navázano spojení s ElasticSearch,
        /// tak je zpřístupněna možnost vyhledávat pomocí této služby.
        /// </summary>
        public MainWindow()
        {
            var settings = new ConnectionSettings(new Uri("http://localhost:9200/"))
               .DefaultIndex("papers")
               .EnableDebugMode()
               .DefaultFieldNameInferrer(s => s)
               .PrettyJson();
            esClient = new ElasticClient(settings);
            var pingRes = esClient.Ping();
            InitializeComponent();
            instance = this;
            if (pingRes.ApiCall.HttpStatusCode.Equals(200))
            {
                btnElasticSearch.IsEnabled = true;
                labelESconnection.Visibility = Visibility.Hidden;
            }
        }

        /// <summary>
        /// Spuštění okna s výsledkem vyhledávání po stisku tlačítka Search.
        /// </summary>
        private void ButtonSearch(object sender, RoutedEventArgs e)
        {
            try
            {
                string vybranySoubor = listBoxSoubory.SelectedItem.ToString();
                string selectedFile = listSouboru[listBoxSoubory.SelectedIndex].pathFileXML + ";" + listSouboru[listBoxSoubory.SelectedIndex].pathFolderPDF;
                Window1 newWindow = new Window1(selectedFile, instance);
                newWindow.Show();
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show("Error: " + ex, "Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        /// <summary>
        /// Přidání cesty k XML souboru do listboxu a listu se soubory
        /// </summary>
        private void ButtonAddFileXML(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Multiselect = true;
            openFileDialog.Filter = "XML files (*.xml)|*.xml";
            if (Properties.Settings.Default.XMLfolder == null)
            {
                openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            }
            else
            {
                openFileDialog.InitialDirectory = Properties.Settings.Default.XMLfolder.ToString();
            }
            if (openFileDialog.ShowDialog() == true)
            {
                foreach (string filename in openFileDialog.FileNames)
                {
                    listSouboru.Add(new ListSouboru(System.IO.Path.GetFullPath(filename), null));
                    listBoxSoubory.Items.Add(listSouboru[listSouboru.Count-1].pathFileXML);
                }
            }
            listBoxSoubory.SelectedIndex = (listBoxSoubory.Items.Count-1);
            if (listBoxSoubory.HasItems)
            {
                btnSearch.IsEnabled = true;
                btnAddFolderPDF.IsEnabled = true;
            }
        }

        /// <summary>
        /// Tlačítko k vyčištění vyhledávacích polí
        /// </summary>
        private void ButtonClear(object sender, RoutedEventArgs e)
        {
            btnClear.IsEnabled = false;
            textBoxAuthors.Clear();
            textBoxTitle.Clear();
            textBoxAbstract.Clear();
        }

        /// <summary>
        /// Nastavení výchozí složky pro otvírání souborů
        /// </summary>
        private void ButtonConfig(object sender, RoutedEventArgs e)
        {
            string folderXML;
            folderXML = ChooseFolder();
            if (folderXML != null)
            {
                Properties.Settings.Default.XMLfolder = folderXML;
                Properties.Settings.Default.Save();
            }
        }

        /// <summary>
        /// Přiřazení složky PDF publikací pro konkrétní ročník
        /// </summary>m>
        private void ButtonAddFolderPDF(object sender, RoutedEventArgs e)
        {
            string folderPDF;
            folderPDF = ChooseFolder();
            listSouboru[listBoxSoubory.SelectedIndex].pathFolderPDF = folderPDF;
        }

        /// <summary>
        /// Metoda pro vybrání složky 
        /// </summary>
        private string ChooseFolder()
        {
            string folderPath;
            OpenFileDialog folderBrowser = new OpenFileDialog();
            folderBrowser.ValidateNames = false;
            folderBrowser.CheckFileExists = false;
            folderBrowser.CheckPathExists = true;
            folderBrowser.FileName = "Folder Selection.";
            
            if (folderBrowser.ShowDialog() == true)
            {
                folderPath = Path.GetDirectoryName(folderBrowser.FileName);
                return folderPath;
            }
            return null;
        }

        /// <summary>
        /// Tlačítko pro otevření okna s vyhledáváním pomocí ElasticSearch
        /// </summary>
        private void ButtonElasticSearch(object sender, RoutedEventArgs e)
        {
            WindowElasticSearch newWindow = new WindowElasticSearch();
            newWindow.Show();
        }
    }
}
