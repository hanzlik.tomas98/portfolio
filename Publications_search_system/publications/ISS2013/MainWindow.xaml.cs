﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Xml;
using System.IO;
using System.Data;
using iText.IO;
using iText.Kernel;
using iText.Layout;
using iText.Forms;
using iText.Kernel.Pdf;
using iText.Kernel.Pdf.Canvas.Parser;
using iText.Kernel.Pdf.Canvas.Parser.Listener;

namespace html_parsing
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {

            InitializeComponent();



        }

        private void Button_Click_Nacti(object sender, RoutedEventArgs e)
        {
            String umisteni = System.IO.Directory.GetCurrentDirectory();
            txtBox_authors.Clear();
            txtBox_papers.Clear();
            txtBox_paths.Clear();
            txtBox_sessions.Clear();

            /*txtBox_autori.Text = "new sample text autori";
            txtBox_prace.Text = "new sample text prace";*/


            //String cesta = "..\\..\\WELCOME.HTM";
            String celaCesta = System.IO.Path.GetFullPath("..\\..\\WELCOME.HTM");

            txtBox_sessions.AppendText(umisteni + "\n\n");
            //txtBox_sessions.AppendText(cesta + "\n\n");
            txtBox_sessions.AppendText(celaCesta);

            String cestaNeni = "..\\..\\neexistuje.HTM";


            List<ListPraci> listPraci = new List<ListPraci>();
            //string[] filePaths = Directory.GetFiles(@"c:\MyDir\", "*.bmp");
            int radek = 1;

            String cesta = "..\\..\\AUTHOR";

            foreach (string file in Directory.EnumerateFiles(cesta, "*.pdf"))
            {

                //StringBuilder text = new StringBuilder();
                if (File.Exists(file))
                {
                    
                    PdfReader pdfReader = new PdfReader(file);


                    //var sText = PdfTextExtractor.GetTextFromPage(pdfReader);

                    /*  iText.Kernel.Crypto.BadPasswordException: Bad user password. 
                     *  Password is not provided or wrong password provided. Correct password should be passed to PdfReader constructor with properties. 
                     *  See ReaderProperties#setPassword() method.
                     */
                    try
                    {
                        var pdfDocument = new PdfDocument(pdfReader);
                        //string info = new PdfDocumentInfo().GetAuthor;
                        PdfDocumentInfo info = pdfDocument.GetDocumentInfo();
                        string pdfInfoAutor = info.GetAuthor();
                        string pdfInfoTitle = info.GetTitle();
                        var page = pdfDocument.GetFirstPage();

                        //SimpleTextExtractionStrategy
                        //LocationTextExtractionStrategy
                        ITextExtractionStrategy strategy = new SimpleTextExtractionStrategy();

                        //error: iText.IO.IOException: The CMap iText.IO.Font.Cmap.UniJIS-UTF16-H was not found.
                        //solution: Encoding CMaps in particular for CJK scripts are in a separate package. For .Net use itext7.font - asian via nuget.
                        string currentText = PdfTextExtractor.GetTextFromPage(page, strategy);
                        currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(
                        Encoding.Default, Encoding.UTF8, Encoding.Default.GetBytes(currentText)));
                        //string textPDF = currentText;
                        var abstrakt = Regex.Match(currentText, ("(?<=Abstract)(.*?)(?=1. Introduction)"), RegexOptions.Singleline); //bez single line cte regex pri "." vsechno az na \n
                                                                                                                                     //good todo - ligatures
                        var pdfCesta1 = Regex.Match(file, ("(?<=AUTHOR)(.*?)(?=.PDF)"));


                        string cestaPDF = "\\IS2013\\PDF\\AUTHOR" + pdfCesta1.ToString() + ".pdf";
                        string abstraktRemoved = Regex.Replace(abstrakt.ToString(), @"\p{C}+", string.Empty);
                        //text.Append(currentText);

                        listPraci.Add(new ListPraci(pdfInfoTitle, pdfInfoAutor, cestaPDF, abstraktRemoved));
                        pdfReader.Close();
                    }
                    catch (iText.Kernel.Crypto.BadPasswordException ex)
                    { 
                        MessageBox.Show("Chyba: " + ex.Message, "Upozornění", MessageBoxButton.OK, MessageBoxImage.Warning);
                        
                    }
                    
                    
                }
                //var abstrakt = Regex.Match(str, "AUTHOR (.*) .pdf"); //matchne vše vč. AUTHOR po .pdf
                //var abstrakt = Regex.Match(str, "Abstract (.*) 1. Introduction", RegexOptions.Singleline);

                /*txtBox_paths.AppendText(radek + ". " + listPraci.Last().cestaPDF + "\n");
                txtBox_papers.AppendText(radek + ". " + listPraci.Last().nazevPrace + "\n");
                txtBox_authors.AppendText(radek + ". " + listPraci.Last().autori + "\n");
                radek++;*/
            }



            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";

            XmlWriter xmlWriter = XmlWriter.Create("ISS2013.xml", settings);
            //XmlWriter xmlWriter = XmlWriter.Create("test.xml");

            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement("conference");

            createXMLHeader(xmlWriter);
            foreach (ListPraci paper in listPraci)
            {
                createXMLNode(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();
            

        }
        private void createXMLHeader(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("conference_name");
            xmlWriter.WriteString("INTERSPEECH 2013");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("date");
            xmlWriter.WriteString("August 25-29th 2013");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("venue");
            xmlWriter.WriteString("Lyon, France");
            xmlWriter.WriteEndElement();
        }
        private void createXMLNode(string paperName, string authors, string path, string abstrakt, XmlWriter xmlWriter)
        {
            string[] poleAutoru = authors.Split(',');

            xmlWriter.WriteStartElement("paper_name");
            xmlWriter.WriteAttributeString("name", paperName);
            xmlWriter.WriteStartElement("authors");
            //for cyklus
            foreach (string autor in poleAutoru)
            {
                xmlWriter.WriteStartElement("author");
                xmlWriter.WriteString(autor);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("abstract");
            xmlWriter.WriteString(abstrakt);
            //todo? ligatury
            //https://stackoverflow.com/questions/29177715/erroran-invalid-xml-character-unicode-0x19-was-found-in-the-element-content?noredirect=1&lq=1
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("file_path");
            xmlWriter.WriteString(path);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        private void Button_Click_Konec(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
