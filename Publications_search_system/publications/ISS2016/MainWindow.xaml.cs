﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Xml;

namespace html_parsing
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {

            InitializeComponent();



        }

        private void Button_Click_Nacti(object sender, RoutedEventArgs e)
        {
            String umisteni = System.IO.Directory.GetCurrentDirectory();
            txtBox_authors.Clear();
            txtBox_papers.Clear();
            txtBox_paths.Clear();
            txtBox_sessions.Clear();

            /*txtBox_autori.Text = "new sample text autori";
            txtBox_prace.Text = "new sample text prace";*/


            String cesta = "..\\..\\WELCOME.HTM";
            String celaCesta = System.IO.Path.GetFullPath("..\\..\\WELCOME.HTM");

            txtBox_sessions.AppendText(umisteni + "\n\n");
            txtBox_sessions.AppendText(cesta + "\n\n");
            txtBox_sessions.AppendText(celaCesta);

            String cestaNeni = "..\\..\\neexistuje.HTM";


            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();

            // There are various options, set as needed
            //htmlDoc.OptionFixNestedTags = true;

            // filePath is a path to a file containing the html
            htmlDoc.Load(cesta, Encoding.UTF8);

            List<ListPraci> listPraci = new List<ListPraci>();
            List<String> listAuthors = new List<String>();
            List<String> listPapers = new List<String>();
            List<String> listCesty = new List<String>();
            // ParseErrors is an ArrayList containing any errors from the Load statement
            if (htmlDoc.ParseErrors != null && htmlDoc.ParseErrors.Count() > 0)
            {

                //error na 7 - rozdílné kódování load streaamu a html dokumentu => při load encoding pro UTF-8
                //error na 7997 - opening tag pro </p> => odstranění </p>

                //error na 6767 - opening tag pro </p> => odstranění </p>
                MessageBox.Show("Parse errors", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);

            }
            else
            {
                if (htmlDoc.DocumentNode != null)
                {
                    HtmlAgilityPack.HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//body");

                    if (bodyNode != null)
                    {
                        int radek = 1;
                        //String classToGet = "CP-PAPER";
                        foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//div[@class='" + "CP-PAPER" + "']"))
                        {
                            /*if (node.Attributes["caption"].Value.Contains("PAPER"))
                            {
                                listPapers.Add(node.Attributes["caption"].Value);
                                txtBox_papers.AppendText(listPapers.Last() + "\n");
                                
                            }*/

                            if(node.SelectNodes(".//span[@class='" + "CP-C-TOCAUTHOR" + "']") != null)
                            {

                            
                            string cestaPDF;
                            String autoriString = "";
                            foreach (HtmlNode node2 in node.SelectNodes(".//span[@class='" + "CP-C-TOCAUTHOR" + "']"))
                            {

                                if (autoriString.Equals(""))
                                {
                                    var output = Regex.Replace(node2.InnerText, @"[\d-]", string.Empty);
                                    autoriString = output;
                                    //autoriString = node2.SelectSingleNode(".//#text").InnerText;
                                }
                                else
                                {
                                    var output = Regex.Replace(node2.InnerText, @"[\d-]", string.Empty);
                                    autoriString = autoriString + ", " + output;
                                }
                            }

                            string abstrakt = node.SelectSingleNode(".//p[@class='" + "CP-C-ABS" + "']").InnerText;

                            
                            if (node.SelectSingleNode(".//a[@href]") != null) 
                            { 
                                cestaPDF = node.SelectSingleNode(".//a[@href]").Attributes["href"].Value;
                            }
                            else
                            {
                                cestaPDF = "PDF file was not found";
                            }
                                string nazevPaper = node.SelectSingleNode(".//h4").InnerText;

                                listPraci.Add(new ListPraci(nazevPaper, autoriString, cestaPDF, abstrakt));

                                txtBox_paths.AppendText(radek + ". " + listPraci.Last().cestaPDF + "\n");
                                txtBox_papers.AppendText(radek + ". " + listPraci.Last().nazevPrace + "\n");
                                txtBox_authors.AppendText(radek + ". " + listPraci.Last().autori + "\n");


                                radek++;
                            }
                            /* if (node.Attributes["pdf_file_full_name"] != null)
                             {
                                 if (node.Attributes["pdf_file_full_name"].Value.Contains("AUTHOR"))
                                 {
                                     if (node.Attributes["caption"] != null)
                                     {
                                         if (node.Attributes["caption"].Value.Contains("PAPER"))
                                         {
                                             var textNode = node.InnerText;
                                             var autori = Regex.Matches(textNode, @"\[([^[|]*)\|");
                                             //String autoriString = "";
                                             foreach (Match m in autori)
                                             {
                                                 if (autoriString.Equals(""))
                                                 {
                                                     autoriString = m.Groups[1].ToString();
                                                 }
                                                 else
                                                 {
                                                     autoriString = autoriString + ", " + m.Groups[1].ToString();
                                                 }
                                             }

                                             var abstrakt = Regex.Match(textNode, ("(?<=cpabstractcardabstract&quot;&gt;&lt;p&gt;)(.*?)(?=&lt;)"));
                                             String abstraktString = abstrakt.ToString();

                                             listPraci.Add(new ListPraci(node.Attributes["caption"].Value, autoriString, node.Attributes["pdf_file_full_name"].Value, abstraktString));

                                             txtBox_paths.AppendText(radek + ". " + listPraci.Last().cestaPDF + "\n");
                                             txtBox_papers.AppendText(radek + ". " + listPraci.Last().nazevPrace + "\n");
                                             txtBox_authors.AppendText(radek + ". " + listPraci.Last().autori + "\n");
                                             //txtBox_sessions.AppendText(radek + ". " + listPraci.Last().abstrakt + "\n");

                                             radek++;

                                         }
                                     }
                                 }
                             }*/

                        }


                        XmlWriterSettings settings = new XmlWriterSettings();
                        settings.Indent = true;
                        settings.IndentChars = "\t";
                        
                        XmlWriter xmlWriter = XmlWriter.Create("test.xml", settings);
                        //XmlWriter xmlWriter = XmlWriter.Create("test.xml");

                        xmlWriter.WriteStartDocument();

                        xmlWriter.WriteStartElement("conference");

                        createXMLHeader(xmlWriter);
                        foreach (ListPraci paper in listPraci)
                        {
                            createXMLNode(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, xmlWriter);
                        }
                        xmlWriter.WriteEndElement();
                        xmlWriter.WriteEndDocument();
                        xmlWriter.Close();

                    }
                }
            }

        }
        private void createXMLHeader(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("conference_name");
            xmlWriter.WriteString("INTERSPEECH 2016");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("date");
            xmlWriter.WriteString("September 8-12th 2016");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("venue");
            xmlWriter.WriteString("San Francisco, California");
            xmlWriter.WriteEndElement();
        }
        private void createXMLNode(string paperName, string authors, string path, string abstrakt, XmlWriter xmlWriter)
        {
            string[] poleAutoru = authors.Split(',');

            xmlWriter.WriteStartElement("paper_name");
            xmlWriter.WriteAttributeString("name", paperName);
            xmlWriter.WriteStartElement("authors");
            //for cyklus
            foreach (string autor in poleAutoru)
            {
                xmlWriter.WriteStartElement("author");
                xmlWriter.WriteString(autor);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("abstract");
            xmlWriter.WriteString(abstrakt);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("file_path");
            xmlWriter.WriteString(path);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        private void Button_Click_Konec(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
