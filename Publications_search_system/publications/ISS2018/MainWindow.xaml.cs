﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using HtmlAgilityPack;
using System.Xml;
using System.IO;

namespace html_parsing
{
    /// <summary>
    /// Interakční logika pro MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {

            InitializeComponent();



        }

        private void Button_Click_Nacti(object sender, RoutedEventArgs e)
        {
            String umisteni = System.IO.Directory.GetCurrentDirectory();
            txtBox_authors.Clear();
            txtBox_papers.Clear();
            txtBox_paths.Clear();
            txtBox_sessions.Clear();

            /*txtBox_autori.Text = "new sample text autori";
            txtBox_prace.Text = "new sample text prace";*/


            //String cesta = "..\\..\\WELCOME.HTM";
            String celaCesta = System.IO.Path.GetFullPath("..\\..\\WELCOME.HTM");

            txtBox_sessions.AppendText(umisteni + "\n\n");
            //txtBox_sessions.AppendText(cesta + "\n\n");
            txtBox_sessions.AppendText(celaCesta);

            String cestaNeni = "..\\..\\neexistuje.HTM";


            HtmlAgilityPack.HtmlDocument htmlDoc = new HtmlAgilityPack.HtmlDocument();

            // There are various options, set as needed
            //htmlDoc.OptionFixNestedTags = true;

            // filePath is a path to a file containing the html
            
            //htmlDoc.Load(cesta, Encoding.UTF8);

            List<ListPraci> listPraci = new List<ListPraci>();
            List<String> listAuthors = new List<String>();
            List<String> listPapers = new List<String>();
            List<String> listCesty = new List<String>();
            // ParseErrors is an ArrayList containing any errors from the Load statement

            //string[] filePaths = Directory.GetFiles(@"c:\MyDir\", "*.bmp");
            int radek = 1;
            String cesta = "..\\..\\abstracts";
            foreach (string file in Directory.EnumerateFiles(cesta, "*.html"))
            {
                htmlDoc.Load(file, Encoding.UTF8);



               




                //var abstrakt = Regex.Match(contents, ("(?<=cpabstractcardabstract&quot;&gt;&lt;p&gt;)(.*?)(?=&lt;)"));
                //var autori = Regex.Matches(textNode, @"\[([^[|]*)\|");


                /*if (htmlDoc.ParseErrors != null && htmlDoc.ParseErrors.Count() > 0)
                {

                    //error na 7 - rozdílné kódování load streaamu a html dokumentu => při load encoding pro UTF-8
                    //error na 7997 - opening tag pro </p> => odstranění </p>

                    //error na 6767 - opening tag pro </p> => odstranění </p>
                    MessageBox.Show("Parse errors", "Alert", MessageBoxButton.OK, MessageBoxImage.Error);

                }
                else*/
                {
                    if (htmlDoc.DocumentNode != null)
                    {
                        HtmlAgilityPack.HtmlNode bodyNode = htmlDoc.DocumentNode.SelectSingleNode("//body");

                        if (bodyNode != null)
                        {
                           
                            //String classToGet = "CP-PAPER";
                            foreach (HtmlNode node in htmlDoc.DocumentNode.SelectNodes("//div[@class='" + "w3-container" + "']"))
                            {

                                
                                string nazevPaper = node.SelectSingleNode(".//h4").InnerText;
                                string h4innerHtml = node.ChildNodes[3].InnerHtml;
                                var pdfCesta = Regex.Match(h4innerHtml, ("(?<=IS18PROC)(.*?)(?=.pdf)"));
                                string cestaPDF = "/IS18PROC" + pdfCesta.ToString() + ".pdf";

                                string abstrakt = node.ChildNodes[8].InnerText;

                                string autoriStr = node.ChildNodes[19].InnerText;
                                var autorineco = Regex.Match(autoriStr, ("(?<=author={)(.*?)(?=})"));
                                string replace = ", ";
                                var autoriString = Regex.Replace(autorineco.ToString(), @"\b and\b", replace);



                                listPraci.Add(new ListPraci(nazevPaper, autoriString, cestaPDF, abstrakt));

                                txtBox_paths.AppendText(radek + ". " + listPraci.Last().cestaPDF + "\n");
                                txtBox_papers.AppendText(radek + ". " + listPraci.Last().nazevPrace + "\n");
                                txtBox_authors.AppendText(radek + ". " + listPraci.Last().autori + "\n");


                                radek++;
                            }
                            
                        }
                    }
                }

            }
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";

            XmlWriter xmlWriter = XmlWriter.Create("test.xml", settings);
            //XmlWriter xmlWriter = XmlWriter.Create("test.xml");

            xmlWriter.WriteStartDocument();

            xmlWriter.WriteStartElement("conference");

            createXMLHeader(xmlWriter);
            foreach (ListPraci paper in listPraci)
            {
                createXMLNode(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();

        }
        private void createXMLHeader(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("conference_name");
            xmlWriter.WriteString("INTERSPEECH 2018");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("date");
            xmlWriter.WriteString("September 2-6th 2018");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("venue");
            xmlWriter.WriteString("Hyderabad, India");
            xmlWriter.WriteEndElement();
        }
        private void createXMLNode(string paperName, string authors, string path, string abstrakt, XmlWriter xmlWriter)
        {
            string[] poleAutoru = authors.Split(',');

            xmlWriter.WriteStartElement("paper_name");
            xmlWriter.WriteAttributeString("name", paperName);
            xmlWriter.WriteStartElement("authors");
            //for cyklus
            foreach (string autor in poleAutoru)
            {
                xmlWriter.WriteStartElement("author");
                xmlWriter.WriteString(autor);
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("abstract");
            xmlWriter.WriteString(abstrakt);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("file_path");
            xmlWriter.WriteString(path);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();


        }
        private void Button_Click_Konec(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
