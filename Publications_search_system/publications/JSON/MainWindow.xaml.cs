﻿using Newtonsoft.Json;
using System.Windows;
using System.Xml;

namespace XMLtoJSON_new
{
    /// <summary>
    /// Překonvertování XML do JSON
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void BtnLoad_Click(object sender, RoutedEventArgs e)
        {
            string cesta = System.IO.Directory.GetCurrentDirectory()+"\\ISS2019.json"; //debug dir
            XmlDocument mojeXml = new XmlDocument();
            mojeXml.Load("..\\..\\ISS2019.xml"); 
            string jsonText = JsonConvert.SerializeXmlNode(mojeXml, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(cesta, jsonText);
        }
    }
}
