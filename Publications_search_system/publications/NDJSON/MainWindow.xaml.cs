﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;
using System.IO;
using Newtonsoft.Json;
using System.Linq;

namespace html_parsing
{
    /// <summary>
    /// Program umoznujici nacteni XML a konverzi do formatu
    /// JSON a "NDJSON" (nutne pro ElasticSearch)
    /// </summary>
    public partial class MainWindow : Window
    {
        List<ListPraci> listPraci = new List<ListPraci>();

        string umisteni = System.IO.Directory.GetCurrentDirectory();

        string loadXMLpath = "..\\..\\ISS2011.xml";
        string aktualniRocnik = "ISS2011";

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_XML(object sender, RoutedEventArgs e)
        {
            
            txtBox_authors.Clear();
            txtBox_papers.Clear();
            txtBox_paths.Clear();
            txtBox_sessions.Clear();

            XmlDocument mojeXml = new XmlDocument();
            mojeXml.Load(loadXMLpath);
            foreach (XmlNode node in mojeXml.SelectNodes("//paper_name"))
            {

                string nazevPaper = node.Attributes["name"].Value;
                var jmenoAutori = node.SelectSingleNode("authors");
                string autoriPrace = "";
                foreach (XmlNode autor in jmenoAutori.ChildNodes)
                {

                    if (autoriPrace.Equals(""))
                    {
                        autoriPrace = autor.InnerText;
                    }
                    else
                    {
                        autoriPrace = autoriPrace + ", " + autor.InnerText;
                    }
                }
                string cestaPDF = node.SelectSingleNode("file_path").InnerText;
                string abstrakt = node.SelectSingleNode("abstract").InnerText;
                listPraci.Add(new ListPraci(nazevPaper, autoriPrace, cestaPDF, abstrakt));
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";

            XmlWriter xmlWriter = XmlWriter.Create(aktualniRocnik+".xml", settings);

            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("conference");
            createXMLHeader(xmlWriter);
            foreach (ListPraci paper in listPraci)
            {
                createXMLNode(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();

        }
        private void createXMLHeader(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("conference_name");
            xmlWriter.WriteString("INTERSPEECH 2013");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("date");
            xmlWriter.WriteString("August 27-31st 2011");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("venue");
            xmlWriter.WriteString("Florence, Italy");
            xmlWriter.WriteEndElement();
        }
        private void createXMLNode(string paperName, string authors, string path, string abstrakt, XmlWriter xmlWriter)
        {
            string[] poleAutoru = authors.Split(',');

            xmlWriter.WriteStartElement("paper");
            xmlWriter.WriteStartElement("title");
            xmlWriter.WriteString(paperName);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("authors");
            foreach (string autor in poleAutoru)
            {
                xmlWriter.WriteStartElement("author");
                xmlWriter.WriteString(autor.Trim());
                xmlWriter.WriteEndElement();
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("abstract");
            xmlWriter.WriteString(abstrakt);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("file_path");
            xmlWriter.WriteString(path);
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }

        private void Button_Click_JSON(object sender, RoutedEventArgs e)
        {
            XmlDocument mojeXml = new XmlDocument();
            mojeXml.Load(loadXMLpath);
            string jsonText = JsonConvert.SerializeXmlNode(mojeXml, Newtonsoft.Json.Formatting.Indented);
            System.IO.File.WriteAllText(umisteni + "\\" + aktualniRocnik + ".json", jsonText);
        }

        private void Button_NDJSON_index(object sender, RoutedEventArgs e)
        {
            int i = 1;
            using (StreamWriter sw = File.CreateText(System.IO.Directory.GetCurrentDirectory() + "\\"+aktualniRocnik+"ESformat.json"))
            {
                foreach (ListPraci paper in listPraci)
                {
                    //index
                    //{"index":{"_index":"schools", "_type":"school", "_id":"1"}}
                    //{ "index": {"_index": "products", "_type": "product", "_id": 1} }
                    sw.WriteLine("{\"index\":{\"_index\":\"papers\",\"_type\":\"paper\",\"_id\":\"" + i + "\"}}");
                    i++;
                    string title = paper.nazevPrace;
                    string[] poleAutoru = paper.autori.Split(',');
                    string autori = "";
                    string posledniAutor = poleAutoru.Last();
                    foreach (string autor in poleAutoru)
                    {
                        autori += "\"" + autor.Trim() + "\"";
                        if (!(autor.Equals(posledniAutor)))
                        {
                            autori += ",";
                        }
                    }

                    sw.WriteLine(
                        "{\"title\":\"" + paper.nazevPrace + "\"," +
                        "\"authors\":[" + autori + "]," +
                        "\"abstract\":\"" + paper.abstrakt + "\"," +
                        "\"file_path\":\"" + paper.cestaPDF + "\"}");

                }
            }
        }
    }
}
