﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Xml;

namespace html_parsing
{
    /// <summary>
    /// Převod starší verze mého xml formátu do nové
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click_Nacti(object sender, RoutedEventArgs e)
        {
            List<ListPraci> listPraci = new List<ListPraci>();
            XmlDocument mojeXml = new XmlDocument();
            mojeXml.Load("..\\..\\ISS2019.xml");
            foreach (XmlNode node in mojeXml.SelectNodes("//paper_name"))
            {
                
                string nazevPaper = node.Attributes["name"].Value;
                var jmenoAutori = node.SelectSingleNode("authors");
                string autoriPrace = "";
                foreach (XmlNode autor in jmenoAutori.ChildNodes)
                {
                   
                    if (autoriPrace.Equals(""))
                    {
                        autoriPrace = autor.InnerText;
                    }
                    else
                    {
                        autoriPrace = autoriPrace +", " + autor.InnerText;
                    }
                }  
                string cestaPDF = node.SelectSingleNode("file_path").InnerText;
                string abstrakt = node.SelectSingleNode("abstract").InnerText;
                listPraci.Add(new ListPraci(nazevPaper, autoriPrace, cestaPDF, abstrakt));
            }

            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = "\t";
            XmlWriter xmlWriter = XmlWriter.Create("ISS2019.xml", settings);
            xmlWriter.WriteStartDocument();
            xmlWriter.WriteStartElement("conference");
            createXMLHeader(xmlWriter);
            foreach (ListPraci paper in listPraci)
            {
                createXMLNode(paper.nazevPrace, paper.autori, paper.cestaPDF, paper.abstrakt, xmlWriter);
            }
            xmlWriter.WriteEndElement();
            xmlWriter.WriteEndDocument();
            xmlWriter.Close();

        }
        private void createXMLHeader(XmlWriter xmlWriter)
        {
            xmlWriter.WriteStartElement("conference_name");
            xmlWriter.WriteString("INTERSPEECH 2019");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("date");
            xmlWriter.WriteString("September 15–19th 2019");
            xmlWriter.WriteEndElement();
            xmlWriter.WriteStartElement("venue");
            xmlWriter.WriteString("Graz, Austria");
            xmlWriter.WriteEndElement();
        }
        private void createXMLNode(string paperName, string authors, string path, string abstrakt, XmlWriter xmlWriter)
        {
            string[] poleAutoru = authors.Split(',');
            xmlWriter.WriteStartElement("paper");
                xmlWriter.WriteStartElement("title");
                    xmlWriter.WriteString(paperName);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("authors");
                    foreach (string autor in poleAutoru)
                    {
                        xmlWriter.WriteStartElement("author");
                        xmlWriter.WriteString(autor.Trim());
                        xmlWriter.WriteEndElement();
                    }
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("abstract");
                    xmlWriter.WriteString(abstrakt);
                xmlWriter.WriteEndElement();
                xmlWriter.WriteStartElement("file_path");
                    xmlWriter.WriteString(path);
                xmlWriter.WriteEndElement();
            xmlWriter.WriteEndElement();
        }
        private void Button_Click_Konec(object sender, RoutedEventArgs e)
        {
            Close();
        }
    }
}
