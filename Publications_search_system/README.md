# Bakalářská práce během studia na Technické univerzitě v Liberci

## 3 hlavní části
1) Protěžba dat (html,xml,pdf)
2) Vytvoření aplikace pro vyhledávání v získaných datech
3) Implementace služby ElasticSearch

## info
MainWindow je výchozí okno po spuštění.
Z něj možnost otevřít ElasticSearch okno při navázání spojení.
Z obou těchto oken je pak po stisknutí tlačítka k vyhledávání 
zobrazeno okno Window1 s výsledky.

## Elasticsearch
Elasticsearch je v fulltextový vyhledávač vycházející z Apache Lucene.
Zjednodušeně si jej lze představit jako noSQL databázi. 
Funguje jako velmi rychlý textový vyhledávač, který umí vypsat nejenom hledaná data,
ale i informace o nich, tedy například kolikrát byl nalezen záznam konkrétního vyhledávání.