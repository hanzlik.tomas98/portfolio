# Zápočtový program pro předmět Teorie grafů a her

## Zadání
V libovolném jazyce implementovat program, který pro daný obraz reprezentován pixely
v odstínech šedi odliší pixely vyobrazeného objektu od kontrastního pozadí.

Celé zadání je v souboru Zadani.png

## info
Jednou z nutností u tohoto programu bylo zajistit rychlostní optimalizaci tak,
aby po nahrání kódu na testovací web proběhl dostatečně rychle.

Shrnutí postupu je v souboru Segmentace_obrazu_Hanzlik.pdf
