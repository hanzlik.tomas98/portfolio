﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TGH_segmentace
{
    class Program
    {
        static int num;
        static string staticString;
        static char[] staticChars;
        static void Main(string[] args)
        {
            
            /*
            num = int.Parse(Console.ReadLine());
            InputArray inputArray = new InputArray(num);
            string line;
            while ((line = Console.ReadLine()) != null)
            {
                inputArray.addStringArray(line);
            }*/
            
            num = int.Parse(Console.ReadLine());
            InputArray inputArray = new InputArray(num);
            
            inputArray.addStringArray("9 9 9 9 9 9");
            inputArray.addStringArray("8 8 6 6 8 8");
            inputArray.addStringArray("7 4 5 5 4 7");
            inputArray.addStringArray("6 3 3 3 3 6");
            inputArray.addStringArray("5 5 2 2 5 5");
            inputArray.addStringArray("4 4 4 4 4 4");
            
            /*
            inputArray.addStringArray("213 213 213 213 213 214 214 215 214 214 214 213 213 214 213 213");
            inputArray.addStringArray("213 213 213 213 213 213 213 214 213 213 213 213 213 213 213 213");
            inputArray.addStringArray("213 212 212 213 213 154 154 155 154 154 213 213 213 213 213 213");
            inputArray.addStringArray("213 212 212 213 153 154 154 155 154 154 153 213 212 212 212 212");
            inputArray.addStringArray("212 212 212 153 152 153 153 154 153 153 152 153 212 212 212 212");
            inputArray.addStringArray("212 212 212 153 152 152 153 154 153 152 152 153 212 212 212 212");
            inputArray.addStringArray("212 212 212 152 152 152 152 154 153 152 152 152 212 212 212 212");
            inputArray.addStringArray("212 212 212 152 152 153 153 154 153 152 152 152 212 212 212 212");
            inputArray.addStringArray("212 212 212 152 152 152 152 153 152 151 151 151 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 152 152 152 152 151 151 151 212 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 212 152 151 151 151 151 212 212 212 212 212 212");
            inputArray.addStringArray("213 212 212 212 212 212 212 212 212 212 212 212 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 212 212 212 212 212 212 212 212 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 212 212 212 212 212 212 212 212 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 212 212 212 212 212 212 212 212 212 212 212 212");
            inputArray.addStringArray("212 212 212 212 212 212 212 212 212 212 212 212 212 212 212 212");
            */

            //vytvreni instance grafu s danymy rozmery
            Graph graph = new Graph(inputArray.getVerticesCount(), inputArray.getEdgesCount());
            //graph.edge[0].Corner = 1;
            //graph.edge[1].Corner = 1;


            string currentLine, nextLine = null;
            IEnumerable<int> nextLineInt2 = null;
            int counter = 0; 
            //ziskani hran grafu z vstupniho 2D pole
            for (int i = 0; i < num; i++)
            {
                //nacti prvni radek z grafu
                currentLine = inputArray.getArray()[i];
                var currentLineInt2 = currentLine.Split(' ').Select(x => int.Parse(x));
                //nacti nasledujici radek pokud nejsem na konci
                if (i<num-1)
                {
                    nextLine = inputArray.getArray()[i + 1];
                    nextLineInt2 = nextLine.Split(' ').Select(x => int.Parse(x));
                }
                
                //projdi sloupce
                for (int j = 0; j < num; j++)
                {
                    if (j<num-1)
                    {
                        //zjisti a zapis hodnotu hrany smerem vpravo (vodorovne)
                        int edgeSidewayValue = Math.Abs(currentLineInt2.ElementAt(j) - currentLineInt2.ElementAt(j+1));
                        graph.edge[counter].Weight = edgeSidewayValue;
                        graph.edge[counter].Source = i*num+j;
                        graph.edge[counter].Destination = i * num + j+1;
                        counter++;
                    }
                    if (i<num-1)
                    {
                        //zjisti a zapis hodnotu hrany smerem dolu (svisle)
                        int edgeDownwardsValue = Math.Abs(currentLineInt2.ElementAt(j) - nextLineInt2.ElementAt(j));
                        graph.edge[counter].Weight = edgeDownwardsValue;
                        graph.edge[counter].Source = i * num + j;
                        graph.edge[counter].Destination = (i+1) * num + j;
                        counter++;
                    }
                }
            }

            //zavolani algoritmu pro zjisteni minimalni kostry grafu
            Kruskal(graph);
        }
        
        //najdi (rekurzivne) predka uzlu
        private static int Find(Subset[] subsets, int i)
        {
            //je uzel svym predkem? (dosazeni konce)
            if (subsets[i].Parent != i)
            {
                //hledej predka a vrat zpatky s jeho hodnotou
                subsets[i].Parent = Find(subsets, subsets[i].Parent);
            }
            //subsets[i].Child = i

            return subsets[i].Parent;
        }

        //spoj uzly dohromady nastavenim hodnoty predka
        private static void Union(Subset[] subsets, int x, int y, Edge result)
        {
            
            int xroot = Find(subsets, x);
            int yroot = Find(subsets, y);
            
            if (subsets[xroot].Rank < subsets[yroot].Rank)
            {
                subsets[xroot].Parent = yroot;
            }
            else if (subsets[xroot].Rank > subsets[yroot].Rank)
            { 
                subsets[yroot].Parent = xroot;
            }
            else
            {
                subsets[yroot].Parent = xroot;
                subsets[xroot].Rank++;
            }
        }
        public static void Kruskal(Graph graph)
        {
            int verticesCount = graph.VerticesCount;
            Edge[] result = new Edge[verticesCount-1];
            int i = 0;
            int e = 0;

            //serazeni hran
            Array.Sort(graph.edge, delegate (Edge a, Edge b)
            {
                return a.Weight.CompareTo(b.Weight);
            });

            //vytvoreni disjuktnich uzlu
            Subset[] subsets = new Subset[verticesCount];
            for (int j = 0; j < verticesCount; j++)
            {
                subsets[j] = new Subset();
            }
            for (int v = 0; v < verticesCount; v++)
            {
                subsets[v].Parent = v;
                subsets[v].Rank = 0;
            }


            while (e < verticesCount - 1) 
            {
                //vyber (nasledujici) hranu
                Edge nextEdge = graph.edge[i++];
                //pro vrchol A najdi predky
                int x = Find(subsets, nextEdge.Source);
                //pro vrchol b najdi predky
                int y = Find(subsets, nextEdge.Destination);

                //pokud predci nejsou stejni, tedy netvori cyklus/kruh
                if (x != y)
                {
                    result[e] = nextEdge;
                    Union(subsets, x, y, result[e]);
                    e++;
                }
            }
            
            //init pole navaznosti vrcholu
            Node[] treeArray = new Node[result.Length+1];
            for (int j = 0; j < result.Length+1; j++)
            {
                treeArray[j] = new Node();
            }


            //int indexOfCorner;
            //zapisuju vsechny navaznosti mezi vrcholy
            for (int j = 0; j < result.Length-1; j++)
            {
                for (int k = 0; k < 4; k++)
                {
                    if (treeArray[result[j].Source].Destinations[k] == 0)
                    {
                        treeArray[result[j].Source].Destinations[k] = result[j].Destination;
                        break;
                    }
                }
                for (int k = 0; k < 4; k++)
                {
                    if (treeArray[result[j].Destination].Destinations[k] == 0)
                    {
                        treeArray[result[j].Destination].Destinations[k] = result[j].Source;
                        break;
                    }
                }
                /*if (result[j].Corner == 1)
                {
                    indexOfCorner = j;
                }*/
            }

            //init pole charu pro vyslednou matici
            staticChars = new char[verticesCount];
            for (int k = 0; k < verticesCount; k++)
            {
                staticChars[k] = '1';
            }
            staticChars[0] = '0';
            RecursionOfTree(treeArray, 0, 0);
            

            //vypis vysledne matice 
            int counter = 0;
            for (int j = 0; j < num; j++)
            {
                for (int k = 0; k < num; k++)
                {
                    Console.Write(staticChars[counter]);
                    Console.Write(" ");
                    counter++;
                }
                Console.Write("\n");
            }
        }

        //metoda rekurzivnho pruchodu polem nodu (vrcholu)
        //zacinam od kraje, vim ze to je pozadi. Kamkoli odtud dojdu tak propisuji 0 do pole vyslednych charu
        public static void  RecursionOfTree(Node[] treeArray, int destination, int source)
        {
            int counter = 0;
            do
            {
                if (treeArray[destination].Destinations[counter]!=0)
                {
                    if (treeArray[destination].Destinations[counter] != source)
                    {
                        staticChars[treeArray[destination].Destinations[counter]] = '0';
                        //staticString = staticString.Substring(0, treeArray[destination].Destinations[counter]) + '0' + staticString.Substring(treeArray[destination].Destinations[counter] + 1);
                        RecursionOfTree(treeArray, treeArray[destination].Destinations[counter],destination);
                    }
                }
                counter++;
                if (counter == 4)
                {
                    break;
                }
            } while (treeArray[destination].Destinations[counter]!=0);

        }

        private static void Print(Edge[] result, int e)
        {
            for (int i = 0; i < e; ++i)
                Console.WriteLine("{0} -- {1} == {2}", result[i].Source, result[i].Destination, result[i].Weight);
        }
        public class InputArray
        {
            private int row;
            private int arraySize;
            private string[] inputArray;
            private int edgesCount;
            private int verticesCount;
            public InputArray(int arraySize)
            {
                this.arraySize = arraySize;
                edgesCount = ((arraySize - 1) * arraySize) + (arraySize * (arraySize - 1));
                verticesCount = arraySize * arraySize;
                inputArray = new string[arraySize];
                row = 0;
            }
            public void addStringArray(string inputArrayString)
            {
                inputArray[row] = inputArrayString;
                row++;
            }
            public int getEdgesCount()
            {
                return edgesCount;
            }
            public int getVerticesCount()
            {
                return verticesCount;
            }
            public string[] getArray()
            {

                return inputArray;
            }
            public string getLineOfIndex(int index)
            {
                return inputArray[index];
            }
        }
        public class Edge
        {
            public int Source;
            public int Destination;
            public int Weight;
            //public int Corner;
        }

        public class Graph
        {
            public int VerticesCount;
            public int EdgesCount;
            public Edge[] edge;
            public Graph(int vertices, int edges)
            {
                this.VerticesCount = vertices;
                this.EdgesCount = edges;
                this.edge = new Edge[edges];
                for (int i = 0; i < edges; i++)
                {
                    edge[i] = new Edge();
                }
            }
        }

        public class Subset
        {
            public int Parent;
            public int Rank;
        }
         
        public class Node
        {
            public int[] Destinations { get; set; }
            public Node()
            {
                Destinations = new int[4];
            }
        }
    }
}
