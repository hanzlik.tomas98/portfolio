﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUI_1
{
    class Mereni
    {
        private double[,] poleNamerenychHodnot;
        private string aktualniStrana;
        private string prvniStrana;
        private string typCocky;
        private string poznamkaLeva;
        private string poznamkaPrava;
        private int pocetMereni;

        public Mereni(double[,] poleNamerenychHodnot, string aktualniStrana, string prvniStrana, string typCocky, string poznamkaLeva, string poznamkaPrava, int pocetMereni)
        {
            this.poleNamerenychHodnot = poleNamerenychHodnot;
            this.aktualniStrana = aktualniStrana;
            this.prvniStrana = prvniStrana;
            this.typCocky = typCocky;
            this.poznamkaLeva = poznamkaLeva;
            this.poznamkaPrava = poznamkaPrava;
            this.pocetMereni = pocetMereni;
        }
        public double[,] getpoleNamerenychHodnot()
        {
            return poleNamerenychHodnot;
        }
        public string getAktualniStrana()
        {
            return aktualniStrana;
        }
        public string getPrvniStrana()
        {
            return prvniStrana;
        }
        public string getTypCocky()
        {
            return typCocky;
        }
        public string getPoznamkaLeva()
        {
            return poznamkaLeva;
        }
        public string getPoznamkaPrava()
        {
            return poznamkaPrava;
        }
        public int getPocetMereni()
        {
            return pocetMereni;
        }
        public void setPoleNamerenychHodnot(double[,] poleNamerenychHodnot)
        {
            this.poleNamerenychHodnot = poleNamerenychHodnot;
        }
    }
}
