﻿namespace GUI_1
{
    partial class Form1
    {
        /// <summary>
        /// Vyžaduje se proměnná návrháře.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Uvolněte všechny používané prostředky.
        /// </summary>
        /// <param name="disposing">hodnota true, když by se měl spravovaný prostředek odstranit; jinak false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kód generovaný Návrhářem Windows Form

        /// <summary>
        /// Metoda vyžadovaná pro podporu Návrháře - neupravovat
        /// obsah této metody v editoru kódu.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBoxPrijmeni = new System.Windows.Forms.TextBox();
            this.comboBoxTypCocky = new System.Windows.Forms.ComboBox();
            this.listBoxVyrobniLinka = new System.Windows.Forms.ListBox();
            this.listBoxMereni = new System.Windows.Forms.ListBox();
            this.listBoxPrvniCocka = new System.Windows.Forms.ListBox();
            this.listBoxObeStrany = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(76, 70);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Příjmení";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(388, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Výrobní linka HC";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(599, 56);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Měření";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(76, 176);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(116, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Výběr typu čočky";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(388, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "První čočka";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(599, 172);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Zadávání obou stran";
            // 
            // textBoxPrijmeni
            // 
            this.textBoxPrijmeni.Location = new System.Drawing.Point(79, 107);
            this.textBoxPrijmeni.Name = "textBoxPrijmeni";
            this.textBoxPrijmeni.Size = new System.Drawing.Size(100, 22);
            this.textBoxPrijmeni.TabIndex = 6;
            // 
            // comboBoxTypCocky
            // 
            this.comboBoxTypCocky.FormattingEnabled = true;
            this.comboBoxTypCocky.Location = new System.Drawing.Point(79, 213);
            this.comboBoxTypCocky.Name = "comboBoxTypCocky";
            this.comboBoxTypCocky.Size = new System.Drawing.Size(230, 24);
            this.comboBoxTypCocky.TabIndex = 7;
            this.comboBoxTypCocky.SelectedIndexChanged += new System.EventHandler(this.ComboBoxTypCocky_SelectedIndexChanged);
            // 
            // listBoxVyrobniLinka
            // 
            this.listBoxVyrobniLinka.FormattingEnabled = true;
            this.listBoxVyrobniLinka.ItemHeight = 16;
            this.listBoxVyrobniLinka.Items.AddRange(new object[] {
            "1",
            "2"});
            this.listBoxVyrobniLinka.Location = new System.Drawing.Point(391, 93);
            this.listBoxVyrobniLinka.Name = "listBoxVyrobniLinka";
            this.listBoxVyrobniLinka.Size = new System.Drawing.Size(120, 36);
            this.listBoxVyrobniLinka.TabIndex = 8;
            // 
            // listBoxMereni
            // 
            this.listBoxMereni.FormattingEnabled = true;
            this.listBoxMereni.ItemHeight = 16;
            this.listBoxMereni.Items.AddRange(new object[] {
            "Start výroby",
            "Opakovaný zápis"});
            this.listBoxMereni.Location = new System.Drawing.Point(602, 95);
            this.listBoxMereni.Name = "listBoxMereni";
            this.listBoxMereni.Size = new System.Drawing.Size(120, 36);
            this.listBoxMereni.TabIndex = 9;
            // 
            // listBoxPrvniCocka
            // 
            this.listBoxPrvniCocka.FormattingEnabled = true;
            this.listBoxPrvniCocka.ItemHeight = 16;
            this.listBoxPrvniCocka.Items.AddRange(new object[] {
            "Levá",
            "Pravá"});
            this.listBoxPrvniCocka.Location = new System.Drawing.Point(391, 213);
            this.listBoxPrvniCocka.Name = "listBoxPrvniCocka";
            this.listBoxPrvniCocka.Size = new System.Drawing.Size(120, 36);
            this.listBoxPrvniCocka.TabIndex = 10;
            // 
            // listBoxObeStrany
            // 
            this.listBoxObeStrany.FormattingEnabled = true;
            this.listBoxObeStrany.ItemHeight = 16;
            this.listBoxObeStrany.Items.AddRange(new object[] {
            "Ano",
            "Ne"});
            this.listBoxObeStrany.Location = new System.Drawing.Point(602, 213);
            this.listBoxObeStrany.Name = "listBoxObeStrany";
            this.listBoxObeStrany.Size = new System.Drawing.Size(120, 36);
            this.listBoxObeStrany.TabIndex = 11;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.button1.Location = new System.Drawing.Point(602, 301);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(120, 40);
            this.button1.TabIndex = 12;
            this.button1.Text = "Pokračovat";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button2.Location = new System.Drawing.Point(602, 357);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(121, 40);
            this.button2.TabIndex = 13;
            this.button2.Text = "Konec";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(796, 446);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.listBoxObeStrany);
            this.Controls.Add(this.listBoxPrvniCocka);
            this.Controls.Add(this.listBoxMereni);
            this.Controls.Add(this.listBoxVyrobniLinka);
            this.Controls.Add(this.comboBoxTypCocky);
            this.Controls.Add(this.textBoxPrijmeni);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Nastaveni";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBoxPrijmeni;
        private System.Windows.Forms.ComboBox comboBoxTypCocky;
        private System.Windows.Forms.ListBox listBoxVyrobniLinka;
        private System.Windows.Forms.ListBox listBoxMereni;
        private System.Windows.Forms.ListBox listBoxPrvniCocka;
        private System.Windows.Forms.ListBox listBoxObeStrany;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}

