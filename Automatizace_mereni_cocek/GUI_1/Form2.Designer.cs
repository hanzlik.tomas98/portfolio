﻿namespace GUI_1
{
    partial class Form2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.pictureBoxLeva = new System.Windows.Forms.PictureBox();
            this.textBoxPoznamka = new System.Windows.Forms.TextBox();
            this.buttonUlozit = new System.Windows.Forms.Button();
            this.uzivatelText = new System.Windows.Forms.Label();
            this.datumText = new System.Windows.Forms.Label();
            this.casText = new System.Windows.Forms.Label();
            this.linkaText = new System.Windows.Forms.Label();
            this.mereniText = new System.Windows.Forms.Label();
            this.stranaCocky = new System.Windows.Forms.Label();
            this.typCocky = new System.Windows.Forms.Label();
            this.toleranceText = new System.Windows.Forms.Label();
            this.Bod1Text = new System.Windows.Forms.Label();
            this.Hodnota1Text = new System.Windows.Forms.Label();
            this.buttonNove = new System.Windows.Forms.Button();
            this.buttonOpakovat = new System.Windows.Forms.Button();
            this.buttonUkoncit = new System.Windows.Forms.Button();
            this.Bod2Text = new System.Windows.Forms.Label();
            this.Hodnota2Text = new System.Windows.Forms.Label();
            this.Bod3Text = new System.Windows.Forms.Label();
            this.Hodnota3Text = new System.Windows.Forms.Label();
            this.Bod4Text = new System.Windows.Forms.Label();
            this.Hodnota4Text = new System.Windows.Forms.Label();
            this.Bod5Text = new System.Windows.Forms.Label();
            this.Hodnota5Text = new System.Windows.Forms.Label();
            this.Bod6Text = new System.Windows.Forms.Label();
            this.Hodnota6Text = new System.Windows.Forms.Label();
            this.Bod7Text = new System.Windows.Forms.Label();
            this.Hodnota7Text = new System.Windows.Forms.Label();
            this.Bod8Text = new System.Windows.Forms.Label();
            this.Hodnota8Text = new System.Windows.Forms.Label();
            this.Bod9Text = new System.Windows.Forms.Label();
            this.Hodnota9Text = new System.Windows.Forms.Label();
            this.Bod10Text = new System.Windows.Forms.Label();
            this.Hodnota10Text = new System.Windows.Forms.Label();
            this.Bod11Text = new System.Windows.Forms.Label();
            this.Hodnota11Text = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.druhaToleranceBodyText = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.druhaToleranceText = new System.Windows.Forms.Label();
            this.sekundarniTolerancePanel = new System.Windows.Forms.Panel();
            this.pictureBoxPrava = new System.Windows.Forms.PictureBox();
            this.pictureBoxTextLeva = new System.Windows.Forms.Label();
            this.pictureBoxTextPrava = new System.Windows.Forms.Label();
            this.pictureBoxSpecialni = new System.Windows.Forms.PictureBox();
            this.pictureBoxTextSpecialni = new System.Windows.Forms.Label();
            this.specialniTolerancePanel = new System.Windows.Forms.Panel();
            this.label14 = new System.Windows.Forms.Label();
            this.tretiToleranceText = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.tretiToleranceBodyText = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeva)).BeginInit();
            this.sekundarniTolerancePanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrava)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialni)).BeginInit();
            this.specialniTolerancePanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 32);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "Uživatel";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(124, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 17);
            this.label2.TabIndex = 1;
            this.label2.Text = "Datum";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(216, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Čas";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(296, 32);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(112, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Výrobní linka HC";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(444, 32);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Měření";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(31, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(142, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Strana měřené čočky";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label7.Location = new System.Drawing.Point(32, 207);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(72, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Typ čočky";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label8.Location = new System.Drawing.Point(31, 295);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 17);
            this.label8.TabIndex = 7;
            this.label8.Text = "Tolerance [μm]";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label9.Location = new System.Drawing.Point(378, 135);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(51, 17);
            this.label9.TabIndex = 8;
            this.label9.Text = "Měření";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label10.Location = new System.Drawing.Point(474, 135);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(93, 17);
            this.label10.TabIndex = 9;
            this.label10.Text = "Hodnota [μm]";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(39, 530);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(132, 17);
            this.label11.TabIndex = 10;
            this.label11.Text = "Poznámka k měření";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(102, 662);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(192, 17);
            this.label12.TabIndex = 11;
            this.label12.Text = "Aktuální hodnota měření [μm]";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(121, 707);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(139, 31);
            this.label13.TabIndex = 12;
            this.label13.Text = "*Hodnota*";
            // 
            // pictureBoxLeva
            // 
            this.pictureBoxLeva.Location = new System.Drawing.Point(612, 42);
            this.pictureBoxLeva.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxLeva.Name = "pictureBoxLeva";
            this.pictureBoxLeva.Size = new System.Drawing.Size(600, 270);
            this.pictureBoxLeva.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLeva.TabIndex = 13;
            this.pictureBoxLeva.TabStop = false;
            // 
            // textBoxPoznamka
            // 
            this.textBoxPoznamka.Location = new System.Drawing.Point(32, 550);
            this.textBoxPoznamka.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxPoznamka.Multiline = true;
            this.textBoxPoznamka.Name = "textBoxPoznamka";
            this.textBoxPoznamka.Size = new System.Drawing.Size(216, 87);
            this.textBoxPoznamka.TabIndex = 14;
            this.textBoxPoznamka.Leave += new System.EventHandler(this.TextBoxPoznamka_Leave);
            // 
            // buttonUlozit
            // 
            this.buttonUlozit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.buttonUlozit.Location = new System.Drawing.Point(314, 694);
            this.buttonUlozit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonUlozit.Name = "buttonUlozit";
            this.buttonUlozit.Size = new System.Drawing.Size(153, 68);
            this.buttonUlozit.TabIndex = 15;
            this.buttonUlozit.Text = "Uložit hodnotu";
            this.buttonUlozit.UseVisualStyleBackColor = false;
            this.buttonUlozit.Click += new System.EventHandler(this.ButtonUlozit_Click);
            // 
            // uzivatelText
            // 
            this.uzivatelText.AutoSize = true;
            this.uzivatelText.Location = new System.Drawing.Point(33, 68);
            this.uzivatelText.Name = "uzivatelText";
            this.uzivatelText.Size = new System.Drawing.Size(23, 17);
            this.uzivatelText.TabIndex = 19;
            this.uzivatelText.Text = "---";
            // 
            // datumText
            // 
            this.datumText.AutoSize = true;
            this.datumText.Location = new System.Drawing.Point(124, 68);
            this.datumText.Name = "datumText";
            this.datumText.Size = new System.Drawing.Size(23, 17);
            this.datumText.TabIndex = 20;
            this.datumText.Text = "---";
            // 
            // casText
            // 
            this.casText.AutoSize = true;
            this.casText.Location = new System.Drawing.Point(216, 68);
            this.casText.Name = "casText";
            this.casText.Size = new System.Drawing.Size(23, 17);
            this.casText.TabIndex = 21;
            this.casText.Text = "---";
            // 
            // linkaText
            // 
            this.linkaText.AutoSize = true;
            this.linkaText.Location = new System.Drawing.Point(339, 68);
            this.linkaText.Name = "linkaText";
            this.linkaText.Size = new System.Drawing.Size(23, 17);
            this.linkaText.TabIndex = 22;
            this.linkaText.Text = "---";
            // 
            // mereniText
            // 
            this.mereniText.AutoSize = true;
            this.mereniText.Location = new System.Drawing.Point(444, 68);
            this.mereniText.Name = "mereniText";
            this.mereniText.Size = new System.Drawing.Size(23, 17);
            this.mereniText.TabIndex = 23;
            this.mereniText.Text = "---";
            // 
            // stranaCocky
            // 
            this.stranaCocky.AutoSize = true;
            this.stranaCocky.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.stranaCocky.Location = new System.Drawing.Point(31, 167);
            this.stranaCocky.Name = "stranaCocky";
            this.stranaCocky.Size = new System.Drawing.Size(92, 20);
            this.stranaCocky.TabIndex = 24;
            this.stranaCocky.Text = "*pravá/levá*";
            // 
            // typCocky
            // 
            this.typCocky.AutoSize = true;
            this.typCocky.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.typCocky.Location = new System.Drawing.Point(32, 235);
            this.typCocky.Name = "typCocky";
            this.typCocky.Size = new System.Drawing.Size(143, 20);
            this.typCocky.TabIndex = 25;
            this.typCocky.Text = "*Název typu čočky*";
            // 
            // toleranceText
            // 
            this.toleranceText.AutoSize = true;
            this.toleranceText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.toleranceText.Location = new System.Drawing.Point(32, 321);
            this.toleranceText.Name = "toleranceText";
            this.toleranceText.Size = new System.Drawing.Size(44, 17);
            this.toleranceText.TabIndex = 26;
            this.toleranceText.Text = "*x - y*";
            // 
            // Bod1Text
            // 
            this.Bod1Text.AutoSize = true;
            this.Bod1Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod1Text.Location = new System.Drawing.Point(377, 179);
            this.Bod1Text.Name = "Bod1Text";
            this.Bod1Text.Size = new System.Drawing.Size(46, 17);
            this.Bod1Text.TabIndex = 27;
            this.Bod1Text.Text = "Bod A";
            // 
            // Hodnota1Text
            // 
            this.Hodnota1Text.AutoSize = true;
            this.Hodnota1Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota1Text.Location = new System.Drawing.Point(474, 179);
            this.Hodnota1Text.Name = "Hodnota1Text";
            this.Hodnota1Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota1Text.TabIndex = 28;
            this.Hodnota1Text.Text = "---";
            // 
            // buttonNove
            // 
            this.buttonNove.Location = new System.Drawing.Point(46, 805);
            this.buttonNove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonNove.Name = "buttonNove";
            this.buttonNove.Size = new System.Drawing.Size(123, 52);
            this.buttonNove.TabIndex = 29;
            this.buttonNove.Text = "Nové nastavení";
            this.buttonNove.UseVisualStyleBackColor = true;
            this.buttonNove.Click += new System.EventHandler(this.ButtonNove_Click);
            // 
            // buttonOpakovat
            // 
            this.buttonOpakovat.Location = new System.Drawing.Point(195, 805);
            this.buttonOpakovat.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonOpakovat.Name = "buttonOpakovat";
            this.buttonOpakovat.Size = new System.Drawing.Size(112, 52);
            this.buttonOpakovat.TabIndex = 30;
            this.buttonOpakovat.Text = "Opakovat měření";
            this.buttonOpakovat.UseVisualStyleBackColor = true;
            this.buttonOpakovat.Click += new System.EventHandler(this.ButtonOpakovat_Click);
            // 
            // buttonUkoncit
            // 
            this.buttonUkoncit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.buttonUkoncit.Location = new System.Drawing.Point(355, 805);
            this.buttonUkoncit.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonUkoncit.Name = "buttonUkoncit";
            this.buttonUkoncit.Size = new System.Drawing.Size(112, 52);
            this.buttonUkoncit.TabIndex = 31;
            this.buttonUkoncit.Text = "Ukončit";
            this.buttonUkoncit.UseVisualStyleBackColor = false;
            this.buttonUkoncit.Click += new System.EventHandler(this.ButtonUkoncit_Click);
            // 
            // Bod2Text
            // 
            this.Bod2Text.AutoSize = true;
            this.Bod2Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod2Text.Location = new System.Drawing.Point(378, 218);
            this.Bod2Text.Name = "Bod2Text";
            this.Bod2Text.Size = new System.Drawing.Size(46, 17);
            this.Bod2Text.TabIndex = 32;
            this.Bod2Text.Text = "Bod B";
            // 
            // Hodnota2Text
            // 
            this.Hodnota2Text.AutoSize = true;
            this.Hodnota2Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota2Text.Location = new System.Drawing.Point(474, 218);
            this.Hodnota2Text.Name = "Hodnota2Text";
            this.Hodnota2Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota2Text.TabIndex = 33;
            this.Hodnota2Text.Text = "---";
            // 
            // Bod3Text
            // 
            this.Bod3Text.AutoSize = true;
            this.Bod3Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod3Text.Location = new System.Drawing.Point(378, 252);
            this.Bod3Text.Name = "Bod3Text";
            this.Bod3Text.Size = new System.Drawing.Size(46, 17);
            this.Bod3Text.TabIndex = 34;
            this.Bod3Text.Text = "Bod C";
            // 
            // Hodnota3Text
            // 
            this.Hodnota3Text.AutoSize = true;
            this.Hodnota3Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota3Text.Location = new System.Drawing.Point(474, 252);
            this.Hodnota3Text.Name = "Hodnota3Text";
            this.Hodnota3Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota3Text.TabIndex = 35;
            this.Hodnota3Text.Text = "---";
            // 
            // Bod4Text
            // 
            this.Bod4Text.AutoSize = true;
            this.Bod4Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod4Text.Location = new System.Drawing.Point(378, 286);
            this.Bod4Text.Name = "Bod4Text";
            this.Bod4Text.Size = new System.Drawing.Size(47, 17);
            this.Bod4Text.TabIndex = 36;
            this.Bod4Text.Text = "Bod D";
            // 
            // Hodnota4Text
            // 
            this.Hodnota4Text.AutoSize = true;
            this.Hodnota4Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota4Text.Location = new System.Drawing.Point(473, 286);
            this.Hodnota4Text.Name = "Hodnota4Text";
            this.Hodnota4Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota4Text.TabIndex = 37;
            this.Hodnota4Text.Text = "---";
            // 
            // Bod5Text
            // 
            this.Bod5Text.AutoSize = true;
            this.Bod5Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod5Text.Location = new System.Drawing.Point(378, 322);
            this.Bod5Text.Name = "Bod5Text";
            this.Bod5Text.Size = new System.Drawing.Size(46, 17);
            this.Bod5Text.TabIndex = 38;
            this.Bod5Text.Text = "Bod E";
            // 
            // Hodnota5Text
            // 
            this.Hodnota5Text.AutoSize = true;
            this.Hodnota5Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota5Text.Location = new System.Drawing.Point(474, 322);
            this.Hodnota5Text.Name = "Hodnota5Text";
            this.Hodnota5Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota5Text.TabIndex = 39;
            this.Hodnota5Text.Text = "---";
            // 
            // Bod6Text
            // 
            this.Bod6Text.AutoSize = true;
            this.Bod6Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod6Text.Location = new System.Drawing.Point(378, 357);
            this.Bod6Text.Name = "Bod6Text";
            this.Bod6Text.Size = new System.Drawing.Size(45, 17);
            this.Bod6Text.TabIndex = 40;
            this.Bod6Text.Text = "Bod F";
            // 
            // Hodnota6Text
            // 
            this.Hodnota6Text.AutoSize = true;
            this.Hodnota6Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota6Text.Location = new System.Drawing.Point(474, 357);
            this.Hodnota6Text.Name = "Hodnota6Text";
            this.Hodnota6Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota6Text.TabIndex = 41;
            this.Hodnota6Text.Text = "---";
            // 
            // Bod7Text
            // 
            this.Bod7Text.AutoSize = true;
            this.Bod7Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod7Text.Location = new System.Drawing.Point(378, 392);
            this.Bod7Text.Name = "Bod7Text";
            this.Bod7Text.Size = new System.Drawing.Size(48, 17);
            this.Bod7Text.TabIndex = 42;
            this.Bod7Text.Text = "Bod G";
            // 
            // Hodnota7Text
            // 
            this.Hodnota7Text.AutoSize = true;
            this.Hodnota7Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota7Text.Location = new System.Drawing.Point(474, 392);
            this.Hodnota7Text.Name = "Hodnota7Text";
            this.Hodnota7Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota7Text.TabIndex = 43;
            this.Hodnota7Text.Text = "---";
            // 
            // Bod8Text
            // 
            this.Bod8Text.AutoSize = true;
            this.Bod8Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod8Text.Location = new System.Drawing.Point(378, 427);
            this.Bod8Text.Name = "Bod8Text";
            this.Bod8Text.Size = new System.Drawing.Size(47, 17);
            this.Bod8Text.TabIndex = 44;
            this.Bod8Text.Text = "Bod H";
            // 
            // Hodnota8Text
            // 
            this.Hodnota8Text.AutoSize = true;
            this.Hodnota8Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota8Text.Location = new System.Drawing.Point(474, 427);
            this.Hodnota8Text.Name = "Hodnota8Text";
            this.Hodnota8Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota8Text.TabIndex = 45;
            this.Hodnota8Text.Text = "---";
            // 
            // Bod9Text
            // 
            this.Bod9Text.AutoSize = true;
            this.Bod9Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod9Text.Location = new System.Drawing.Point(378, 462);
            this.Bod9Text.Name = "Bod9Text";
            this.Bod9Text.Size = new System.Drawing.Size(40, 17);
            this.Bod9Text.TabIndex = 46;
            this.Bod9Text.Text = "Bod I";
            // 
            // Hodnota9Text
            // 
            this.Hodnota9Text.AutoSize = true;
            this.Hodnota9Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota9Text.Location = new System.Drawing.Point(474, 462);
            this.Hodnota9Text.Name = "Hodnota9Text";
            this.Hodnota9Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota9Text.TabIndex = 47;
            this.Hodnota9Text.Text = "---";
            // 
            // Bod10Text
            // 
            this.Bod10Text.AutoSize = true;
            this.Bod10Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod10Text.Location = new System.Drawing.Point(378, 497);
            this.Bod10Text.Name = "Bod10Text";
            this.Bod10Text.Size = new System.Drawing.Size(44, 17);
            this.Bod10Text.TabIndex = 48;
            this.Bod10Text.Text = "Bod J";
            // 
            // Hodnota10Text
            // 
            this.Hodnota10Text.AutoSize = true;
            this.Hodnota10Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota10Text.Location = new System.Drawing.Point(474, 497);
            this.Hodnota10Text.Name = "Hodnota10Text";
            this.Hodnota10Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota10Text.TabIndex = 49;
            this.Hodnota10Text.Text = "---";
            // 
            // Bod11Text
            // 
            this.Bod11Text.AutoSize = true;
            this.Bod11Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Bod11Text.Location = new System.Drawing.Point(378, 531);
            this.Bod11Text.Name = "Bod11Text";
            this.Bod11Text.Size = new System.Drawing.Size(46, 17);
            this.Bod11Text.TabIndex = 50;
            this.Bod11Text.Text = "Bod K";
            // 
            // Hodnota11Text
            // 
            this.Hodnota11Text.AutoSize = true;
            this.Hodnota11Text.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Hodnota11Text.Location = new System.Drawing.Point(473, 531);
            this.Hodnota11Text.Name = "Hodnota11Text";
            this.Hodnota11Text.Size = new System.Drawing.Size(23, 17);
            this.Hodnota11Text.TabIndex = 51;
            this.Hodnota11Text.Text = "---";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label21.Location = new System.Drawing.Point(-1, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 17);
            this.label21.TabIndex = 52;
            this.label21.Text = "Pro body: ";
            // 
            // druhaToleranceBodyText
            // 
            this.druhaToleranceBodyText.AutoSize = true;
            this.druhaToleranceBodyText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.druhaToleranceBodyText.Location = new System.Drawing.Point(68, 0);
            this.druhaToleranceBodyText.Name = "druhaToleranceBodyText";
            this.druhaToleranceBodyText.Size = new System.Drawing.Size(113, 17);
            this.druhaToleranceBodyText.TabIndex = 53;
            this.druhaToleranceBodyText.Text = "*jednotlivé body*";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label38.Location = new System.Drawing.Point(-1, 28);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(125, 17);
            this.label38.TabIndex = 54;
            this.label38.Text = "Platí tolerance[μm]";
            // 
            // druhaToleranceText
            // 
            this.druhaToleranceText.AutoSize = true;
            this.druhaToleranceText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.druhaToleranceText.Location = new System.Drawing.Point(-1, 51);
            this.druhaToleranceText.Name = "druhaToleranceText";
            this.druhaToleranceText.Size = new System.Drawing.Size(44, 17);
            this.druhaToleranceText.TabIndex = 55;
            this.druhaToleranceText.Text = "*x - y*";
            // 
            // sekundarniTolerancePanel
            // 
            this.sekundarniTolerancePanel.Controls.Add(this.label38);
            this.sekundarniTolerancePanel.Controls.Add(this.druhaToleranceText);
            this.sekundarniTolerancePanel.Controls.Add(this.label21);
            this.sekundarniTolerancePanel.Controls.Add(this.druhaToleranceBodyText);
            this.sekundarniTolerancePanel.Location = new System.Drawing.Point(34, 357);
            this.sekundarniTolerancePanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.sekundarniTolerancePanel.Name = "sekundarniTolerancePanel";
            this.sekundarniTolerancePanel.Size = new System.Drawing.Size(204, 69);
            this.sekundarniTolerancePanel.TabIndex = 56;
            // 
            // pictureBoxPrava
            // 
            this.pictureBoxPrava.Location = new System.Drawing.Point(612, 336);
            this.pictureBoxPrava.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxPrava.Name = "pictureBoxPrava";
            this.pictureBoxPrava.Size = new System.Drawing.Size(600, 270);
            this.pictureBoxPrava.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxPrava.TabIndex = 57;
            this.pictureBoxPrava.TabStop = false;
            // 
            // pictureBoxTextLeva
            // 
            this.pictureBoxTextLeva.AutoSize = true;
            this.pictureBoxTextLeva.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pictureBoxTextLeva.Location = new System.Drawing.Point(631, 25);
            this.pictureBoxTextLeva.Name = "pictureBoxTextLeva";
            this.pictureBoxTextLeva.Size = new System.Drawing.Size(70, 15);
            this.pictureBoxTextLeva.TabIndex = 58;
            this.pictureBoxTextLeva.Text = "Levá strana";
            // 
            // pictureBoxTextPrava
            // 
            this.pictureBoxTextPrava.AutoSize = true;
            this.pictureBoxTextPrava.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pictureBoxTextPrava.Location = new System.Drawing.Point(631, 319);
            this.pictureBoxTextPrava.Name = "pictureBoxTextPrava";
            this.pictureBoxTextPrava.Size = new System.Drawing.Size(75, 15);
            this.pictureBoxTextPrava.TabIndex = 59;
            this.pictureBoxTextPrava.Text = "Pravá strana";
            // 
            // pictureBoxSpecialni
            // 
            this.pictureBoxSpecialni.Location = new System.Drawing.Point(612, 625);
            this.pictureBoxSpecialni.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBoxSpecialni.Name = "pictureBoxSpecialni";
            this.pictureBoxSpecialni.Size = new System.Drawing.Size(600, 270);
            this.pictureBoxSpecialni.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSpecialni.TabIndex = 60;
            this.pictureBoxSpecialni.TabStop = false;
            // 
            // pictureBoxTextSpecialni
            // 
            this.pictureBoxTextSpecialni.AutoSize = true;
            this.pictureBoxTextSpecialni.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.pictureBoxTextSpecialni.Location = new System.Drawing.Point(631, 608);
            this.pictureBoxTextSpecialni.Name = "pictureBoxTextSpecialni";
            this.pictureBoxTextSpecialni.Size = new System.Drawing.Size(58, 15);
            this.pictureBoxTextSpecialni.TabIndex = 61;
            this.pictureBoxTextSpecialni.Text = "Speciální";
            // 
            // specialniTolerancePanel
            // 
            this.specialniTolerancePanel.Controls.Add(this.label14);
            this.specialniTolerancePanel.Controls.Add(this.tretiToleranceText);
            this.specialniTolerancePanel.Controls.Add(this.label16);
            this.specialniTolerancePanel.Controls.Add(this.tretiToleranceBodyText);
            this.specialniTolerancePanel.Location = new System.Drawing.Point(32, 445);
            this.specialniTolerancePanel.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.specialniTolerancePanel.Name = "specialniTolerancePanel";
            this.specialniTolerancePanel.Size = new System.Drawing.Size(204, 69);
            this.specialniTolerancePanel.TabIndex = 62;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(-1, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(125, 17);
            this.label14.TabIndex = 54;
            this.label14.Text = "Platí tolerance[μm]";
            // 
            // tretiToleranceText
            // 
            this.tretiToleranceText.AutoSize = true;
            this.tretiToleranceText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tretiToleranceText.Location = new System.Drawing.Point(-1, 51);
            this.tretiToleranceText.Name = "tretiToleranceText";
            this.tretiToleranceText.Size = new System.Drawing.Size(44, 17);
            this.tretiToleranceText.TabIndex = 55;
            this.tretiToleranceText.Text = "*x - y*";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label16.Location = new System.Drawing.Point(-1, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(73, 17);
            this.label16.TabIndex = 52;
            this.label16.Text = "Pro body: ";
            // 
            // tretiToleranceBodyText
            // 
            this.tretiToleranceBodyText.AutoSize = true;
            this.tretiToleranceBodyText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.tretiToleranceBodyText.Location = new System.Drawing.Point(68, 0);
            this.tretiToleranceBodyText.Name = "tretiToleranceBodyText";
            this.tretiToleranceBodyText.Size = new System.Drawing.Size(113, 17);
            this.tretiToleranceBodyText.TabIndex = 53;
            this.tretiToleranceBodyText.Text = "*jednotlivé body*";
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(1224, 921);
            this.Controls.Add(this.specialniTolerancePanel);
            this.Controls.Add(this.pictureBoxTextSpecialni);
            this.Controls.Add(this.pictureBoxSpecialni);
            this.Controls.Add(this.pictureBoxTextPrava);
            this.Controls.Add(this.pictureBoxTextLeva);
            this.Controls.Add(this.pictureBoxPrava);
            this.Controls.Add(this.sekundarniTolerancePanel);
            this.Controls.Add(this.Hodnota11Text);
            this.Controls.Add(this.Bod11Text);
            this.Controls.Add(this.Hodnota10Text);
            this.Controls.Add(this.Bod10Text);
            this.Controls.Add(this.Hodnota9Text);
            this.Controls.Add(this.Bod9Text);
            this.Controls.Add(this.Hodnota8Text);
            this.Controls.Add(this.Bod8Text);
            this.Controls.Add(this.Hodnota7Text);
            this.Controls.Add(this.Bod7Text);
            this.Controls.Add(this.Hodnota6Text);
            this.Controls.Add(this.Bod6Text);
            this.Controls.Add(this.Hodnota5Text);
            this.Controls.Add(this.Bod5Text);
            this.Controls.Add(this.Hodnota4Text);
            this.Controls.Add(this.Bod4Text);
            this.Controls.Add(this.Hodnota3Text);
            this.Controls.Add(this.Bod3Text);
            this.Controls.Add(this.Hodnota2Text);
            this.Controls.Add(this.Bod2Text);
            this.Controls.Add(this.buttonUkoncit);
            this.Controls.Add(this.buttonOpakovat);
            this.Controls.Add(this.buttonNove);
            this.Controls.Add(this.Hodnota1Text);
            this.Controls.Add(this.Bod1Text);
            this.Controls.Add(this.toleranceText);
            this.Controls.Add(this.typCocky);
            this.Controls.Add(this.stranaCocky);
            this.Controls.Add(this.mereniText);
            this.Controls.Add(this.linkaText);
            this.Controls.Add(this.casText);
            this.Controls.Add(this.datumText);
            this.Controls.Add(this.uzivatelText);
            this.Controls.Add(this.buttonUlozit);
            this.Controls.Add(this.textBoxPoznamka);
            this.Controls.Add(this.pictureBoxLeva);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "Form2";
            this.Text = "Mereni vrstvy HC";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLeva)).EndInit();
            this.sekundarniTolerancePanel.ResumeLayout(false);
            this.sekundarniTolerancePanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxPrava)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSpecialni)).EndInit();
            this.specialniTolerancePanel.ResumeLayout(false);
            this.specialniTolerancePanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.PictureBox pictureBoxLeva;
        private System.Windows.Forms.TextBox textBoxPoznamka;
        private System.Windows.Forms.Button buttonUlozit;
        private System.Windows.Forms.Label uzivatelText;
        private System.Windows.Forms.Label datumText;
        private System.Windows.Forms.Label casText;
        private System.Windows.Forms.Label linkaText;
        private System.Windows.Forms.Label mereniText;
        private System.Windows.Forms.Label stranaCocky;
        private System.Windows.Forms.Label typCocky;
        private System.Windows.Forms.Label toleranceText;
        private System.Windows.Forms.Label Bod1Text;
        private System.Windows.Forms.Label Hodnota1Text;
        private System.Windows.Forms.Button buttonNove;
        private System.Windows.Forms.Button buttonOpakovat;
        private System.Windows.Forms.Button buttonUkoncit;
        private System.Windows.Forms.Label Bod2Text;
        private System.Windows.Forms.Label Hodnota2Text;
        private System.Windows.Forms.Label Bod3Text;
        private System.Windows.Forms.Label Hodnota3Text;
        private System.Windows.Forms.Label Bod4Text;
        private System.Windows.Forms.Label Hodnota4Text;
        private System.Windows.Forms.Label Bod5Text;
        private System.Windows.Forms.Label Hodnota5Text;
        private System.Windows.Forms.Label Bod6Text;
        private System.Windows.Forms.Label Hodnota6Text;
        private System.Windows.Forms.Label Bod7Text;
        private System.Windows.Forms.Label Hodnota7Text;
        private System.Windows.Forms.Label Bod8Text;
        private System.Windows.Forms.Label Hodnota8Text;
        private System.Windows.Forms.Label Bod9Text;
        private System.Windows.Forms.Label Hodnota9Text;
        private System.Windows.Forms.Label Bod10Text;
        private System.Windows.Forms.Label Hodnota10Text;
        private System.Windows.Forms.Label Bod11Text;
        private System.Windows.Forms.Label Hodnota11Text;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label druhaToleranceBodyText;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label druhaToleranceText;
        private System.Windows.Forms.Panel sekundarniTolerancePanel;
        private System.Windows.Forms.PictureBox pictureBoxPrava;
        private System.Windows.Forms.Label pictureBoxTextLeva;
        private System.Windows.Forms.Label pictureBoxTextPrava;
        private System.Windows.Forms.PictureBox pictureBoxSpecialni;
        private System.Windows.Forms.Label pictureBoxTextSpecialni;
        private System.Windows.Forms.Panel specialniTolerancePanel;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label tretiToleranceText;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label tretiToleranceBodyText;
    }
}