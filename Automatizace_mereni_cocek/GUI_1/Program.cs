﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GUI_1
{
    static class Program
    {
        private const string jmSouboru = "C:\\Users\\t-hanzlik\\Desktop\\AZSN\\SeznamCocek.csv";

        //public static Nastaveni nastaveni = new Nastaveni(null, null, null, null, null, null, false, null);
        public static Nastaveni nastaveni;
        public static List<Cocka> seznamCocek = new List<Cocka>();
        public static bool noveMereni;

        /// <summary>
        /// Hlavní vstupní bod aplikace.
        /// </summary>
        [STAThread]
        static void Main()
        {
            try
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Nastroje.NactiCockyCSV(jmSouboru);
                do
                {
                    noveMereni = false;
                    Application.Run(new Form1());
                    Application.Run(new Form2());

                } while (noveMereni);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
    }
    }
}
