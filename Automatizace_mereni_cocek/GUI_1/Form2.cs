﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GUI_1
{
    public partial class Form2 : Form
    {
        private const string obrazekCockaCesta = "C:\\Users\\t-hanzlik\\Desktop\\AZSN\\cockyObrazky\\";
        
        private int indexNastavenaCocka;
        private string poznamkaPrava = null;
        private string poznamkaLeva = null;

        public Form2()
        {
            InitializeComponent();
            uzivatelText.Text = Program.nastaveni.getPrijmeni();
            datumText.Text = Program.nastaveni.getDatum();
            casText.Text = Program.nastaveni.getCas();
            linkaText.Text = Program.nastaveni.getVyrobniLinka();
            mereniText.Text = Program.nastaveni.getMereni();
            stranaCocky.Text = Program.nastaveni.getStranaCocky();
            typCocky.Text = Program.nastaveni.getTypCocky();

            /*pictureBoxLeva.Image = Image.FromFile(obrazekCockaCesta + Program.nastaveni.getTypCocky() +" leva.jpg");
            pictureBoxPrava.Image = Image.FromFile(obrazekCockaCesta  + Program.nastaveni.getTypCocky() + " prava.jpg");*/

            for (int i = 0; i < Program.seznamCocek.Count; i++)
            {
                if(Program.nastaveni.getTypCocky().Equals(Program.seznamCocek[i].getTypCocky()))
                {
                    indexNastavenaCocka = i;
                    break;
                }
            }
            NastavViditelnost();
            NastavTolerance(indexNastavenaCocka);
            NactiObrazekCocky(obrazekCockaCesta, "leva");
            NactiObrazekCocky(obrazekCockaCesta, "prava");
            NactiObrazekCocky(obrazekCockaCesta, "specialni");
        }
        
        public void NastavPoleProCocky(int indexNastavenaCocka)
        {
            int pocetBodu = Program.seznamCocek[indexNastavenaCocka].getPocetMereni();
            string[] poleBoduPrava = new string[pocetBodu+1];
            string[] poleBoduLeva = new string[pocetBodu+1]; 
            
                
            


                
        }

        public void NastavTolerance(int indexNastavenaCocka)
        {
            toleranceText.Text = Program.seznamCocek[indexNastavenaCocka].getDolniMez().ToString() + " - " +
                        Program.seznamCocek[indexNastavenaCocka].getHorniMez().ToString();
            if ((Program.seznamCocek[indexNastavenaCocka].getBodySekundarni() != null))
            {
                druhaToleranceText.Text = Program.seznamCocek[indexNastavenaCocka].getDolniMezSekundarni().ToString() + " - " +
                        Program.seznamCocek[indexNastavenaCocka].getHorniMezSekundarni().ToString();

                String[] sekundarniBodyPole = Program.seznamCocek[indexNastavenaCocka].getBodySekundarni();
                String sekundarniBodyRetezec = "";
                /*for (int i = 0; i < Program.seznamCocek[indexNastavenaCocka].getPocetMereniSekundarni(); i++)
                {
                    sekundarniBodyRetezec += String.Join(", ", sekundarniBodyPole[i]);
                }*/
                sekundarniBodyRetezec = String.Join(", ", sekundarniBodyPole);
                druhaToleranceBodyText.Text = sekundarniBodyRetezec;
            }
            else
            {
                sekundarniTolerancePanel.Visible = false;
            }
            if ((Program.seznamCocek[indexNastavenaCocka].getBodySpecialni() != null))
            {
                tretiToleranceText.Text = Program.seznamCocek[indexNastavenaCocka].getDolniMezSpecialni().ToString() + " - " +
                        Program.seznamCocek[indexNastavenaCocka].getHorniMezSpecialni().ToString();

                String[] specialniBodyPole = Program.seznamCocek[indexNastavenaCocka].getBodySpecialni();
                String specialniBodyRetezec = "";
                specialniBodyRetezec = String.Join(", ", specialniBodyPole);
                tretiToleranceBodyText.Text = specialniBodyRetezec;
            }
            else
            {
                specialniTolerancePanel.Visible = false;
            }


        }

        public void NactiObrazekCocky(String obrazekCockaCesta, String cocka) 
        {
            
            try
            {
                switch (cocka)
                {
                    case "leva":
                      pictureBoxLeva.Image =  Image.FromFile(obrazekCockaCesta + Program.nastaveni.getTypCocky() + " " + cocka + ".jpg");
                        break;
                    case "prava":
                      pictureBoxPrava.Image = Image.FromFile(obrazekCockaCesta + Program.nastaveni.getTypCocky() + " " + cocka + ".jpg");
                        break;
                    case "specialni":
                      pictureBoxSpecialni.Image = Image.FromFile(obrazekCockaCesta + Program.nastaveni.getTypCocky() + " " + cocka + ".jpg");
                        break;
                    default:
                        break;
                }
                
                
            } catch (System.IO.FileNotFoundException)
            {
               
                switch (cocka)
                {
                    case "leva":
                        pictureBoxLeva.Visible = false;
                        pictureBoxTextLeva.Visible = false;
                        break;
                    case "prava":
                        pictureBoxPrava.Visible = false;
                        pictureBoxTextPrava.Visible = false;
                        break;
                    case "specialni":
                        pictureBoxSpecialni.Visible = false;
                        pictureBoxTextSpecialni.Visible = false;
                        break;
                    default:
                        break;
                }

            }
        }


        private void ulozDoCSV()
        {
            // Program.seznamCocek[indexNastavenaCocka].getPocetMereni()
            string[] namereneHodnotyAPoznamky = { "6,9", "8,7" };
            Nastroje.UlozDataDoCSV(namereneHodnotyAPoznamky);
        }

        private void NastavViditelnost()
        {


            switch (Program.seznamCocek[indexNastavenaCocka].getPocetMereni())
            {
                case 1:
                    Bod2Text.Hide();
                    Hodnota2Text.Hide();
                    goto case 2;
                case 2:
                    Bod3Text.Hide();
                    Hodnota3Text.Hide();
                    goto case 3;
                case 3:
                    Bod4Text.Hide();
                    Hodnota2Text.Hide();
                    goto case 4;
                case 4:
                    Bod5Text.Hide();
                    Hodnota5Text.Hide();
                    goto case 5;
                case 5:
                    Bod6Text.Hide();
                    Hodnota6Text.Hide();
                    goto case 6;
                case 6:
                    Bod7Text.Hide();
                    Hodnota7Text.Hide();
                    goto case 7;
                case 7:
                    Bod8Text.Hide();
                    Hodnota8Text.Hide();
                    goto case 8;
                case 8:
                    Bod9Text.Hide();
                    Hodnota9Text.Hide();
                    goto case 9;
                case 9:
                    Bod10Text.Hide();
                    Hodnota10Text.Hide();
                    goto case 10;
                case 10:
                    Bod11Text.Hide();
                    Hodnota11Text.Hide();
                    break;
                default:
                    break;                    
            }
        }

        private void TextBoxPoznamka_Leave(object sender, EventArgs e)
        {
            
            switch (Program.nastaveni.getStranaCocky())
            {
                case "Pravá":
                    poznamkaPrava = textBoxPoznamka.Text;
                    break;
                case "Levá":
                    poznamkaLeva = textBoxPoznamka.Text;
                    break;
                default:
                    throw new ArgumentException ("Nepodařilo se zapsat poznámku");
                    
            }

        }

        private void ButtonUkoncit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void ButtonOpakovat_Click(object sender, EventArgs e)
        {
            ulozDoCSV();
            //TBD 
            //vymazat aktualně naměřené hodnoty 
            //zresetovat text ve Formu u bodů
        }

        private void ButtonNove_Click(object sender, EventArgs e)
        {
            Program.noveMereni = true;
            Close();
        }

        private void ButtonUlozit_Click(object sender, EventArgs e)
        {

            //TBD
            //ulozit hodnotu aktualne zmereneho bodu - tedy precist textLabel s hodnotou a tu ulozit(?)
            //udelat metodu tak, aby ulozeni reagovalo i na enter
        }
    }
}
