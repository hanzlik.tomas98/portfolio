﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUI_1
{
    class Nastaveni
    {
        private String prijmeni;
        private String datum;
        private String cas;
        private String vyrobniLinka;
        private String mereni;
        private String typCocky;
        private bool obeStrany;
        private String stranaCocky; 

        public Nastaveni(string prijmeni, string datum, string cas, string vyrobniLinka, string mereni, string typCocky,
                        bool obeStrany, string stranaCocky)
        {
            this.prijmeni = prijmeni;
            this.datum = datum;
            this.cas = cas;
            this.vyrobniLinka = vyrobniLinka;
            this.mereni = mereni;
            this.typCocky = typCocky;
            this.obeStrany = obeStrany;
            this.stranaCocky = stranaCocky;
        }

        public String getPrijmeni()
        {
            return prijmeni;
        }
        public String getDatum()
        {
            return datum;
        }
        public String getCas()
        {
            return cas;
        }
        public String getVyrobniLinka()
        {
            return vyrobniLinka;
        }
        public String getMereni()
        {
            return mereni;
        }
        public String getTypCocky()
        {
            return typCocky;
        }
        public bool getObeStrany()
        {
            return obeStrany;
        }
        public String getStranaCocky()
        {
            return stranaCocky;
        }

        public void setPrijmeni(String prijmeni)
        {
            this.prijmeni = prijmeni;
        }
        public void setDatum(String datum)
        {
            this.datum = datum;
        }
        public void setCas(String cas)
        {
            this.cas = cas;
        }
        public void setVyrobniLinka(String vyrobniLinka)
        {
            this.vyrobniLinka = vyrobniLinka;
        }
        public void setMereni(String mereni)
        {
            this.mereni= mereni;
        }
        public void setTypCocky(String typCocky)
        {
            this.typCocky= typCocky;
        }
        public void setObeStrany(bool obeStrany)
        {
            this.obeStrany= obeStrany;
        }
        public void setStranaCocky(String stranaCocky)
        {
            this.stranaCocky = stranaCocky;
        }

    }
}
