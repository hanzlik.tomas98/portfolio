﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GUI_1
{
    class Nastroje
    {
        //public static readonly string Empty;

        public static void ZkontrolujPrijmeni(string prijmeni)
        {
            if (prijmeni.Length < 2)
                throw new ArgumentException("Příjmení je příliš krátké");


        }
        public static bool PrevedStringNaBoolean(String nastaveniBoolText)
        {

            switch (nastaveniBoolText)
            {
                case "Ano":
                    return true;

                case "Ne":
                    return false;

                default:
                    throw new ArgumentException("Nepodařilo se převést string na bool");
            }
        }


        public static String NastavDruhouStranuCocky(String stranaCocky)
        {
            switch (stranaCocky)
            {
                case "Pravá":
                    return "Levá";
                case "Levá":
                    return "Pravá";
                default:
                    throw new ArgumentException("Nepodařilo se nastavit druhou stranu cocky");
            }


        }
        /*public static String ZapisPoznamku(String poznamka)
        {
            return null;
        }

        public static void NactiObrazekCocky(String jmObrazku)
        {
            
        }*/

        public static void NactiCockyCSV(String jmSouboru)
        {
            // Otevře soubor pro čtení
            String header = "typ_cocky;pocet_merenych_bodu;dolni_mez;horni_mez;pocet_bodu_druhe_tolerance;dolni_mez_2;horni_mez_2;jednotlive_body;pocet_bodu_specialni_tolerance;dolni_mez_3;horni_mez_3;jednotlive_body";
            using (StreamReader sr = new StreamReader(jmSouboru))
            {
                String line = sr.ReadLine();
                if (!line.Equals(header) || line == null)
                {
                    throw new ArgumentException("Chybna hlavicka souboru: " + jmSouboru + "Hlavicka neobsahuje: " + header);
                }

                String s;

                while ((s = sr.ReadLine()) != null && !(s == ""))
                {
                    string[] item = s.Split(';');
                    //string typCocky, int pocetMereni, int dolniMez, int horniMez, 
                    //int pocetMereniSekundarni, int dolniMezSekundarni, int horniMezSekundarni, string[] bodySekundarni
                    string typCocky = item[0];
                    int pocetMereni = int.Parse(item[1]);
                    double dolniMez = double.Parse(item[2]);
                    double horniMez = double.Parse(item[3]);

                    try
                    {
                        int pocetMereniSekundarni = int.Parse(item[4]);
                        string[] bodySekundarni = new string[pocetMereniSekundarni];
                        double dolniMezSekundarni = double.Parse(item[5]);
                        double horniMezSekundarni = double.Parse(item[6]);
                        int polozka = 7;
                        for (int i = 0; i < pocetMereniSekundarni; i++)
                        {
                            bodySekundarni[i] = item[polozka];
                            polozka++;
                        }

                        try
                        {
                            int pocetMereniSpecialni = int.Parse(item[polozka]);
                            string[] bodySpecialni = new string[pocetMereniSpecialni];
                            double dolniMezSpecialni = double.Parse(item[polozka + 1]);
                            double horniMezSpecialni = double.Parse(item[polozka + 2]);
                            for (int i = 0; i < pocetMereniSpecialni; i++)
                            {
                                bodySpecialni[i] = item[polozka + 3 + i];
                            }
                            Program.seznamCocek.Add(new Cocka(typCocky, pocetMereni, dolniMez, horniMez,
                                pocetMereniSekundarni, dolniMezSekundarni, horniMezSekundarni, bodySekundarni, pocetMereniSpecialni, dolniMezSpecialni, horniMezSpecialni, bodySpecialni));
                        }
                        catch (IndexOutOfRangeException)
                        {

                            Program.seznamCocek.Add(new Cocka(typCocky, pocetMereni, dolniMez, horniMez, pocetMereniSekundarni, dolniMezSekundarni, horniMezSekundarni, bodySekundarni, 0, 0, 0, null));
                        }


                    }
                    catch (IndexOutOfRangeException)
                    {
                        Program.seznamCocek.Add(new Cocka(typCocky, pocetMereni, dolniMez, horniMez, 0, 0, 0, null, 0, 0, 0, null));
                    }


                    /*if(!(item[4] == null))
                    {
                        int pocetMereniSekundarni = int.Parse(item[4]);
                        string[] bodySekundarni = new string[pocetMereniSekundarni];
                        double dolniMezSekundarni = double.Parse(item[5]);
                        double horniMezSekundarni = double.Parse(item[6]);
                        for (int i = 0; i < pocetMereniSekundarni; i++)
                        {
                            bodySekundarni[i] = item[7 + i];
                        }
                        Program.seznamCocek.Add(new Cocka(typCocky, pocetMereni, dolniMez, horniMez, pocetMereniSekundarni, dolniMezSekundarni, horniMez, bodySekundarni));
                    }
                    else
                    {
                        Program.seznamCocek.Add(new Cocka(typCocky, pocetMereni, dolniMez, horniMez, 0, 0, 0, null));
                    }*/

                    // přidá uživatele s danými hodnotami
                }
            }


        }
        public static void UlozDataDoCSV(string[] mereni)
        {
            try
            {
                string cesta = "C:\\Users\\t-hanzlik\\Desktop\\AZSN\\cockyCSV\\";
                /*FileStream fs = new FileStream(cesta + Program.nastaveni.getTypCocky() + ".csv", FileMode.Append, FileAccess.Write, FileShare.Write);
                fs.Close();
                using (StreamWriter sw = new StreamWriter(cesta + Program.nastaveni.getTypCocky() + ".csv"))*/
                

                    string[] setUp = { Program.nastaveni.getDatum(), Program.nastaveni.getCas(), Program.nastaveni.getPrijmeni(), Program.nastaveni.getMereni(), Program.nastaveni.getVyrobniLinka()};
                    string nastaveni = String.Join(";", setUp);
                    string namereneBody = String.Join(";", mereni);
                    String radek = nastaveni + ";" + namereneBody;
                    
                    File.AppendAllText(cesta + Program.nastaveni.getTypCocky() + ".csv", radek+"\n");
                MessageBox.Show("Hodnoty byly úspěšně uloženy", "Upozornění", MessageBoxButtons.OK, MessageBoxIcon.Information);
               
            }
            catch (DirectoryNotFoundException)
            {
                throw new DirectoryNotFoundException("Nepodařilo se uložit hodnoty. Chybná cesta pro ukládání");
            }
        }

    }
}
