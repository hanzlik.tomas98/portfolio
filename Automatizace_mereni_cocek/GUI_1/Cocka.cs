﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GUI_1
{
    class Cocka
    {
        private String typCocky;
        private int pocetMereni;
        private double dolniMez;
        private double horniMez;
        //pro specialni pripady. Pokud ne -> zadat nulu 
        private int pocetMereniSekundarni;
        private double dolniMezSekundarni;
        private double horniMezSekundarni;
        private String[] bodySekundarni;
        private int pocetMereniSpecialni;
        private double dolniMezSpecialni;
        private double horniMezSpecialni;
        private String[] bodySpecialni;

        public Cocka(string typCocky, int pocetMereni, double dolniMez, double horniMez, 
                    int pocetMereniSekundarni,double dolniMezSekundarni, double horniMezSekundarni, string[] bodySekundarni,
                    int pocetMereniSpecialni, double dolniMezSpecialni, double horniMezSpecialni, string[] bodySpecialni)
        {
            this.typCocky = typCocky;
            this.pocetMereni = pocetMereni;
            this.dolniMez = dolniMez;
            this.horniMez = horniMez;
            this.pocetMereniSekundarni = pocetMereniSekundarni;
            this.dolniMezSekundarni = dolniMezSekundarni;
            this.horniMezSekundarni = horniMezSekundarni;
            this.bodySekundarni = bodySekundarni;
            this.pocetMereniSpecialni = pocetMereniSpecialni;
            this.dolniMezSpecialni = dolniMezSpecialni;
            this.horniMezSpecialni = horniMezSpecialni;
            this.bodySpecialni = bodySpecialni;
        }

        public String getTypCocky()
        {
            return typCocky;
        }
        public int getPocetMereni()
        {
            return pocetMereni;
        }
        public double getDolniMez()
        {
            return dolniMez;
        }
        public double getHorniMez()
        {
            return horniMez;
        }
        public int getPocetMereniSekundarni()
        {
            return pocetMereniSekundarni;
        }
        public double getDolniMezSekundarni()
        {
            return dolniMezSekundarni;
        }
        public double getHorniMezSekundarni()
        {
            return horniMezSekundarni;
        }
        public String[] getBodySekundarni()
        {          
            return bodySekundarni;
        }
        public int getPocetMereniSpecialni()
        {
            return pocetMereniSpecialni;
        }
        public double getDolniMezSpecialni()
        {
            return dolniMezSpecialni;
        }
        public double getHorniMezSpecialni()
        {
            return horniMezSpecialni;
        }
        public String[] getBodySpecialni()
        {
            return bodySpecialni;
        }

    }
}
