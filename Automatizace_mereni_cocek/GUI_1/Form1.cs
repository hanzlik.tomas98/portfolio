﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GUI_1
{
    public partial class Form1 : Form
    {
        private bool ukonci = true;
        private String cocka;
        public Form1()
        {

            InitializeComponent();
            for (int i = 0; i < Program.seznamCocek.Count; i++)
            {
                comboBoxTypCocky.Items.Add(Program.seznamCocek[i].getTypCocky());
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {

            DateTime time = DateTime.Now;
            DateTime date = DateTime.Today;
            String cas;
            String datum;
            bool obeStrany;
            cas = time.TimeOfDay.ToString();
            cas = cas.Remove(5, cas.Length - 5);
            datum = date.ToShortDateString();
            String prijmeni;
            prijmeni = textBoxPrijmeni.Text;
            

            try
            {
                String mereni = listBoxMereni.SelectedItem.ToString();
                String stranaCocky = listBoxPrvniCocka.SelectedItem.ToString();
                String vyrobniLinka = listBoxVyrobniLinka.SelectedItem.ToString();
                String obeStranyText = listBoxObeStrany.SelectedItem.ToString();
                obeStrany = Nastroje.PrevedStringNaBoolean(obeStranyText);
                Nastroje.ZkontrolujPrijmeni(prijmeni);
                /*Program.nastaveni.setPrijmeni(prijmeni);
                Program.nastaveni.setDatum(datum);
                Program.nastaveni.setCas(cas);
                Program.nastaveni.setObeStrany(obeStrany);
                Program.nastaveni.setVyrobniLinka(vyrobniLinka);
                Program.nastaveni.setPrvniStrana(prvniCocka);
                Program.nastaveni.setMereni(mereni);
                Program.nastaveni.setTypCocky(cocka);*/
                
                Program.nastaveni = new Nastaveni(prijmeni,datum,cas,vyrobniLinka,mereni,cocka,obeStrany,stranaCocky);

                ukonci = false;
                Close();

            }

            catch (NullReferenceException)
            {
                MessageBox.Show("Nebyly zvoleny všechny položky, prosím opakujte", "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Chyba", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }


        }

        private void Button2_Click(object sender, EventArgs e)
        {

            Application.Exit();

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (ukonci == true)
            {
                Application.Exit();
            }
        }

        private void ComboBoxTypCocky_SelectedIndexChanged(object sender, EventArgs e)
        {
            cocka = comboBoxTypCocky.SelectedItem.ToString();
        }
    }
}
